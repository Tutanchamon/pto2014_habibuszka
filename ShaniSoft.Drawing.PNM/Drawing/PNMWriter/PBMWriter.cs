using System;
using System.Drawing;
using ShaniSoft.IO.DataWriter;

namespace ShaniSoft.Drawing.PNMWriter
{
	/// <summary>
	/// Summary description for PBMWriter.
	/// </summary>
	internal class PBMWriter : IPNMWriter
	{		
		#region IPNMWriter Members

		public void WriteImageData(IPNMDataWriter dw, System.Drawing.Image im)
		{
			int i = 0;

			//convert im to grey scale and write to output file
			for(int y=0;y<im.Height;y++)
			{
				for(int x=0;x<im.Width;x++)
				{
					Color c=((Bitmap)im).GetPixel(x,y);
					int luma = (int)((c.R * 11 + c.G * 16 + c.B * 5) / 32);

					if(luma < 127)
						dw.WriteByte((byte)1);
					else
						dw.WriteByte((byte)0);

					i++;

					//one line cannot contain more than 70 chars										
                    if (dw is ASCIIDataWriter && i >= 34)
					{
						i = 0;
						dw.WriteLine(string.Empty);
					}
				}
			}

			dw.Close();
		}

		#endregion
	}
}
