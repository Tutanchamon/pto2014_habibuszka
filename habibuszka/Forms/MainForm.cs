﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ShaniSoft.Drawing;
using habibuszka.Exceptions;
using habibuszka.Transformations;
using habibuszka.Hist;
using habibuszka.Transformations.Segmentation;

namespace habibuszka
{
    public partial class MainForm : Form
    {
        public bool grayScale = false;
        public Image loadedImage;
        public Image modifiedImage;
        private ImageMemory imageMemory; 

        public MainForm()
        {
            InitializeComponent();
            imageMemory = new ImageMemory(undoToolStripMenuItem);
            MorphologicalOperatorTest morTest = new MorphologicalOperatorTest();
            morTest.TestMatrices();
        }

        private void openFileMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    imageMemory = new ImageMemory(undoToolStripMenuItem);
                    pictureBox.Image = PNM.ReadPNM(openFileDialog.FileName);
                    if (openFileDialog.FileName.Substring(openFileDialog.FileName.Length - 4, 4).ToLower().EndsWith(".pgm"))
                    {
                        grayScale = true;
                    }
                    else
                    {
                        grayScale = false;
                    }
                    loadedImage = pictureBox.Image;
                    modifiedImage = (Image)loadedImage.Clone();
                    ZoomScrollBar.Value = 0;
                }
                catch
                {
                    new HabibuszkaMainException("Wystąpił błąd przy próbie otwarcia obrazu");
                }
            }
        }

        private void closeAppMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void HistogramBtn_Click(object sender, EventArgs e)
        {
            HistogramForm histogramForm = new HistogramForm(pictureBox.Image, grayScale);
            histogramForm.Show();
        }

        private void SaveBinaryMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Portable Grayscale Format (.pgm)|*.pgm|Portable Pixmap Format (.ppm)|*.ppm";
            saveFileDialog.ShowDialog();
            if (saveFileDialog.FileName != "")
            {
                try
                {
                    string ext = saveFileDialog.FileName.Substring(saveFileDialog.FileName.Length-4,4).ToLower();
                    switch (ext)
                    {
                        case ".pgm":
                            PNM.WritePNM(saveFileDialog.FileName, pictureBox.Image, PNMEncoding.BinaryEncoding, PNMType.PGM);
                            break;
                        case ".ppm":
                            PNM.WritePNM(saveFileDialog.FileName, pictureBox.Image, PNMEncoding.BinaryEncoding, PNMType.PPM);
                            break;
                    }
                    
                }
                catch (Exception ex)
                {
                    new HabibuszkaMainException(ex.Message);
                }
            }
        }

        private void SaveASCIIMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Portable Bitmap Format (.pbm)|*.pbm|Portable Grayscale Format (.pgm)|*.pgm|Portable Pixmap Format (.ppm)|*.ppm";
            saveFileDialog.ShowDialog();
            if (saveFileDialog.FileName != "")
            {
                try
                {
                    string ext = saveFileDialog.FileName.Substring(saveFileDialog.FileName.Length - 4, 4).ToLower();
                    switch (ext)
                    {
                        case ".pbm":
                            PNM.WritePNM(saveFileDialog.FileName, pictureBox.Image, PNMEncoding.ASCIIEncoding, PNMType.PBM);
                            break;
                        case ".pgm":
                            PNM.WritePNM(saveFileDialog.FileName, pictureBox.Image, PNMEncoding.ASCIIEncoding, PNMType.PGM);
                            break;
                        case ".ppm":
                            PNM.WritePNM(saveFileDialog.FileName, pictureBox.Image, PNMEncoding.ASCIIEncoding, PNMType.PPM);
                            break;
                    }
                }
                catch (Exception ex)
                {
                    new HabibuszkaMainException(ex.Message);
                }
            }
        }

        private void ZoomScrollBar_Scroll(object sender, ScrollEventArgs e)
        {
            int zoom = ZoomScrollBar.Value;
            int width = loadedImage.Width;
            int height = loadedImage.Height;
            Bitmap originalBitmap = new Bitmap(modifiedImage);
            Size newSize;
            if (zoom == 0)
                newSize = new Size((int)(width), (int)(height));
            else 
                newSize = new Size((int)(width * ((100 + (double)zoom) / 100)), (int)(height * ((100 + (double)zoom) / 100)));

            if(newSize.Height == 0 || newSize.Width == 0)
            {
                newSize.Height = 1;
                newSize.Width = 1;
            }
            Bitmap bmp = new Bitmap(originalBitmap, newSize);
            pictureBox.Image = bmp;
            ZoomLabel.Text = (100 + zoom).ToString() + "%";
        }

        private void NegativeBtn_Click(object sender, EventArgs e)
        {
            BackupAndSave(pictureBox.Image);
            NegativeOperation negOperation = new NegativeOperation(pictureBox.Image);
            modifiedImage = negOperation.Execute();
            pictureBox.Image = modifiedImage;
        }

        private void CorrectionBtn_Click(object sender, EventArgs e)
        {
            Image tempImage = (Image)pictureBox.Image.Clone();
            CorrectionForm correctionForm = new CorrectionForm(pictureBox.Image);
            if (correctionForm.ShowDialog() == DialogResult.OK)
            {
                BackupAndSave(tempImage);
                modifiedImage = correctionForm.modifiedImage;
                pictureBox.Image = modifiedImage;
            }
        }

        private void GrayscaleBtn_Click(object sender, EventArgs e)
        {
            BackupAndSave(pictureBox.Image);
            GrayscaleOperation grayOperation = new GrayscaleOperation(pictureBox.Image);
            modifiedImage = grayOperation.Execute();
            pictureBox.Image = modifiedImage;
        }

        private void histogramSplitButton_Click(object sender, EventArgs e)
        {
            HistogramForm histogramForm = new HistogramForm(pictureBox.Image, grayScale);
            histogramForm.Show();
        }

        private void HistogramStretchingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BackupAndSave(pictureBox.Image);
            Histogram histogram = new Histogram(new Bitmap(pictureBox.Image));
            Bitmap newImage = histogram.HistogramStretching();
            BackupAndSave(pictureBox.Image);
            pictureBox.Image = (Image)newImage;
            HistogramForm histForm = new HistogramForm(newImage, grayScale);
            histForm.Show();
        }

        private void HistogramEqualizationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Histogram histogram = new Histogram(new Bitmap(pictureBox.Image));
            Bitmap newImage = histogram.Equalize();
            BackupAndSave(pictureBox.Image);
            pictureBox.Image = (Image)newImage;
            HistogramForm histForm = new HistogramForm(newImage, grayScale);
            histForm.Show();
        }

        private void BackupAndSave(Image image)
        {
            imageMemory.Execute(image);
        }

        private void uniformBlurToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Convolution c = new Convolution(new Bitmap(pictureBox.Image));
            Bitmap img = c.Convolute(BlurUniform.GetMask(1), Mode.RepeatEdge);
            BackupAndSave(img);
        }

        private void gaussianBlurToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GaussianBlurForm gForm = new GaussianBlurForm(pictureBox.Image);
            if (gForm.ShowDialog() == DialogResult.OK)
            {
                BackupAndSave(pictureBox.Image);
                modifiedImage = gForm.modifiedImage;
                pictureBox.Image = modifiedImage;
            }
        }

        private void linearBlurToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LinearBlurForm lbform = new LinearBlurForm(pictureBox.Image);
            if (lbform.ShowDialog() == DialogResult.OK)
            {
                BackupAndSave(pictureBox.Image);
                modifiedImage = lbform.modifiedImage;
                pictureBox.Image = modifiedImage;
            }
        }

        private void manualBinToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ManualBinForm mbForm = new ManualBinForm(pictureBox.Image);
            if (mbForm.ShowDialog() == DialogResult.OK)
            {
                BackupAndSave(pictureBox.Image);
                modifiedImage = mbForm.modifiedImage;
                pictureBox.Image = modifiedImage;
            }
        }

        private void gradientBinToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GradientBin gb = new GradientBin(pictureBox.Image);
            Bitmap image = gb.Execute(gb.CalculateThreshold());
            BackupAndSave(pictureBox.Image);
            pictureBox.Image = (Image)image;
            modifiedImage = image;
            pictureBox.Image = modifiedImage;
        }

        private void FileMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void undoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            pictureBox.Image = imageMemory.Undo(pictureBox.Image);
            if (imageMemory.GetUndoCount() == 0)
            {
                undoToolStripMenuItem.Enabled = false;
                
            }
            redoToolStripMenuItem.Enabled = true;

        }

        private void redoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            pictureBox.Image = imageMemory.Redo(pictureBox.Image);
            if (imageMemory.GetRedoCount() == 0)
            {
                redoToolStripMenuItem.Enabled = false;
                
            }
            undoToolStripMenuItem.Enabled = true;
        }

        private void medianNoiseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            NoiseMedianForm nmForm = new NoiseMedianForm(pictureBox.Image);
            if (nmForm.ShowDialog() == DialogResult.OK)
            {
                BackupAndSave(pictureBox.Image);
                modifiedImage = nmForm.modifiedImage;
                pictureBox.Image = modifiedImage;
            }
        }

        private void bilateralNoiseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            NoiseBilateralForm nbForm = new NoiseBilateralForm(pictureBox.Image);
            if (nbForm.ShowDialog() == DialogResult.OK)
            {
                BackupAndSave(pictureBox.Image);
                modifiedImage = nbForm.modifiedImage;
                pictureBox.Image = modifiedImage;
            }
        }

        private void dilateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MorphologicalForm mForm = new MorphologicalForm(pictureBox.Image, MorpholigicalType.Dilate);
            if (mForm.ShowDialog() == DialogResult.OK)
            {
                BackupAndSave(pictureBox.Image);
                modifiedImage = mForm.modifiedImage;
                pictureBox.Image = modifiedImage;
            }

        }

        private void erodeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MorphologicalForm mForm = new MorphologicalForm(pictureBox.Image, MorpholigicalType.Erode);
            if (mForm.ShowDialog() == DialogResult.OK)
            {
                BackupAndSave(pictureBox.Image);
                modifiedImage = mForm.modifiedImage;
                pictureBox.Image = modifiedImage;
            }
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MorphologicalForm mForm = new MorphologicalForm(pictureBox.Image, MorpholigicalType.Open);
            if (mForm.ShowDialog() == DialogResult.OK)
            {
                BackupAndSave(pictureBox.Image);
                modifiedImage = mForm.modifiedImage;
                pictureBox.Image = modifiedImage;
            }
        }

        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MorphologicalForm mForm = new MorphologicalForm(pictureBox.Image, MorpholigicalType.Close);
            if (mForm.ShowDialog() == DialogResult.OK)
            {
                BackupAndSave(pictureBox.Image);
                modifiedImage = mForm.modifiedImage;
                pictureBox.Image = modifiedImage;
            }
        }

        private void sobelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BackupAndSave(pictureBox.Image);
            EdgeSobel edge = new EdgeSobel(new Bitmap(pictureBox.Image));
            modifiedImage = edge.Transform();
            pictureBox.Image = modifiedImage;
        }

        private void prewittToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BackupAndSave(pictureBox.Image);
            EdgePrewitt edge = new EdgePrewitt(new Bitmap(pictureBox.Image));
            modifiedImage = edge.Transform();
            pictureBox.Image = modifiedImage;
        }

        private void robertsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BackupAndSave(pictureBox.Image);
            EdgeRoberts edge = new EdgeRoberts(new Bitmap(pictureBox.Image));
            modifiedImage = edge.Transform();
            pictureBox.Image = modifiedImage;
        }

        private void laplacianToolStripMenuItem_Click(object sender, EventArgs e)
        {
            EdgeLaplacianForm form = new EdgeLaplacianForm(pictureBox.Image);
            if (form.ShowDialog() == DialogResult.OK)
            {
                BackupAndSave(pictureBox.Image);
                modifiedImage = form.modifiedImage;
                pictureBox.Image = modifiedImage;
            }
        }

        private void zerocrossingLoGToolStripMenuItem_Click(object sender, EventArgs e)
        {
            EdgeZeroForm form = new EdgeZeroForm(pictureBox.Image);
            if (form.ShowDialog() == DialogResult.OK)
            {
                BackupAndSave(pictureBox.Image);
                modifiedImage = form.modifiedImage;
                pictureBox.Image = modifiedImage;
            }
        }

        private void heightMapToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BackupAndSave(pictureBox.Image);
            MapHeight map = new MapHeight(pictureBox.Image);
            modifiedImage = map.Transform();
            pictureBox.Image = modifiedImage;
        }

        private void normalMapToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MapNormalForm form = new MapNormalForm(pictureBox.Image);
            if (form.ShowDialog() == DialogResult.OK)
            {
                BackupAndSave(pictureBox.Image);
                modifiedImage = form.modifiedImage;
                pictureBox.Image = modifiedImage;
            }
        }

        private void horizonMapToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MapHorizonForm form = new MapHorizonForm(pictureBox.Image);
            if (form.ShowDialog() == DialogResult.OK)
            {
                BackupAndSave(pictureBox.Image);
                modifiedImage = form.modifiedImage;
                pictureBox.Image = modifiedImage;
            }
        }

        private void cannyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            EdgeCannyForm form = new EdgeCannyForm(pictureBox.Image);
            if (form.ShowDialog() == DialogResult.OK)
            {
                BackupAndSave(pictureBox.Image);
                modifiedImage = form.modifiedImage;
                pictureBox.Image = modifiedImage;
            }
        }

        private void houghToolStripMenuItem_Click(object sender, EventArgs e)
        {
            HoughForm form = new HoughForm(pictureBox.Image);
            if (form.ShowDialog() == DialogResult.OK)
            {
                BackupAndSave(pictureBox.Image);
                modifiedImage = form.modifiedImage;
                pictureBox.Image = modifiedImage;
            }
        }

        private void houghLinesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            HoughLinesForm form = new HoughLinesForm(pictureBox.Image);
            if (form.ShowDialog() == DialogResult.OK)
            {
                BackupAndSave(pictureBox.Image);
                modifiedImage = form.modifiedImage;
                pictureBox.Image = modifiedImage;
            }
        }

        private void CornerDetectionBtn_Click(object sender, EventArgs e)
        {
            CornerHarrisForm form = new CornerHarrisForm(pictureBox.Image);
            if (form.ShowDialog() == DialogResult.OK)
            {
                BackupAndSave(pictureBox.Image);
                modifiedImage = form.modifiedImage;
                pictureBox.Image = modifiedImage;
            }
        }

        private void houghRectanglesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BackupAndSave(pictureBox.Image);
            HoughRectangles map = new HoughRectangles(new Bitmap(pictureBox.Image));
            modifiedImage = map.Transform();
            pictureBox.Image = modifiedImage;
        }

        private void segmentationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BackupAndSave(pictureBox.Image);
            Segmentation map = new Segmentation(new Bitmap(pictureBox.Image));
            modifiedImage = map.Transform();
            pictureBox.Image = modifiedImage;
        }
    }
}
