﻿using habibuszka.Exceptions;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace habibuszka.Transformations
{
    public partial class NoiseMedianForm : Form
    {
        private Image image;
        public Image modifiedImage;
        public NoiseMedianForm(Image image)
        {
            InitializeComponent();
            this.image = image;
        }

        private void OkButton_Click(object sender, EventArgs e)
        {
            try
            {
                int val = int.Parse(RadiusTextBox.Text);
                NoiseMedian mb = new NoiseMedian(new Bitmap(image), val);
                modifiedImage = (Image)mb.Transform();
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            catch
            {
                new HabibuszkaMainException("Podaj prawidłową wartość promienia.");
            }
        }
    }
}
