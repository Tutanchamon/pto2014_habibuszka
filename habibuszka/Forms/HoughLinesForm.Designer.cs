﻿namespace habibuszka
{
    partial class HoughLinesForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.OkButton = new System.Windows.Forms.Button();
            this.ThresholdTextBox = new System.Windows.Forms.TextBox();
            this.LinesCheckBox = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(44, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 13);
            this.label2.TabIndex = 20;
            this.label2.Text = "threshold";
            // 
            // OkButton
            // 
            this.OkButton.Location = new System.Drawing.Point(173, 153);
            this.OkButton.Name = "OkButton";
            this.OkButton.Size = new System.Drawing.Size(95, 23);
            this.OkButton.TabIndex = 19;
            this.OkButton.Text = "OK";
            this.OkButton.UseVisualStyleBackColor = true;
            this.OkButton.Click += new System.EventHandler(this.OkButton_Click);
            // 
            // ThresholdTextBox
            // 
            this.ThresholdTextBox.Location = new System.Drawing.Point(100, 38);
            this.ThresholdTextBox.Name = "ThresholdTextBox";
            this.ThresholdTextBox.Size = new System.Drawing.Size(100, 20);
            this.ThresholdTextBox.TabIndex = 18;
            this.ThresholdTextBox.Text = "3";
            // 
            // LinesCheckBox
            // 
            this.LinesCheckBox.AutoSize = true;
            this.LinesCheckBox.Location = new System.Drawing.Point(100, 80);
            this.LinesCheckBox.Name = "LinesCheckBox";
            this.LinesCheckBox.Size = new System.Drawing.Size(104, 17);
            this.LinesCheckBox.TabIndex = 22;
            this.LinesCheckBox.Text = "draw whole lines";
            this.LinesCheckBox.UseVisualStyleBackColor = true;
            // 
            // HoughLinesForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(280, 188);
            this.Controls.Add(this.LinesCheckBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.OkButton);
            this.Controls.Add(this.ThresholdTextBox);
            this.Name = "HoughLinesForm";
            this.Text = "HoughLinesForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button OkButton;
        private System.Windows.Forms.TextBox ThresholdTextBox;
        private System.Windows.Forms.CheckBox LinesCheckBox;
    }
}