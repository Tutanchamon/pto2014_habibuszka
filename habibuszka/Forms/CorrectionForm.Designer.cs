﻿namespace habibuszka
{
    partial class CorrectionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BrightnessScrollBar = new System.Windows.Forms.HScrollBar();
            this.ContrastScrollBar = new System.Windows.Forms.HScrollBar();
            this.GammaScrollBar = new System.Windows.Forms.HScrollBar();
            this.PreviewPictureBox = new System.Windows.Forms.PictureBox();
            this.BrightnessLabel = new System.Windows.Forms.Label();
            this.ContrastLabel = new System.Windows.Forms.Label();
            this.GammaLabel = new System.Windows.Forms.Label();
            this.BrightnessValue = new System.Windows.Forms.Label();
            this.ContrastValue = new System.Windows.Forms.Label();
            this.GammaValue = new System.Windows.Forms.Label();
            this.okButton = new System.Windows.Forms.Button();
            this.resetButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.PreviewPictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // BrightnessScrollBar
            // 
            this.BrightnessScrollBar.Location = new System.Drawing.Point(148, 361);
            this.BrightnessScrollBar.Maximum = 255;
            this.BrightnessScrollBar.Minimum = -255;
            this.BrightnessScrollBar.Name = "BrightnessScrollBar";
            this.BrightnessScrollBar.Size = new System.Drawing.Size(251, 17);
            this.BrightnessScrollBar.TabIndex = 0;
            this.BrightnessScrollBar.Scroll += new System.Windows.Forms.ScrollEventHandler(this.BrightnessScrollBar_Scroll);
            // 
            // ContrastScrollBar
            // 
            this.ContrastScrollBar.Location = new System.Drawing.Point(148, 394);
            this.ContrastScrollBar.Maximum = 200;
            this.ContrastScrollBar.Minimum = 1;
            this.ContrastScrollBar.Name = "ContrastScrollBar";
            this.ContrastScrollBar.Size = new System.Drawing.Size(251, 17);
            this.ContrastScrollBar.TabIndex = 1;
            this.ContrastScrollBar.Value = 100;
            this.ContrastScrollBar.Scroll += new System.Windows.Forms.ScrollEventHandler(this.ContrastScrollBar_Scroll);
            // 
            // GammaScrollBar
            // 
            this.GammaScrollBar.Location = new System.Drawing.Point(148, 426);
            this.GammaScrollBar.Maximum = 300;
            this.GammaScrollBar.Name = "GammaScrollBar";
            this.GammaScrollBar.Size = new System.Drawing.Size(251, 17);
            this.GammaScrollBar.TabIndex = 2;
            this.GammaScrollBar.Value = 100;
            this.GammaScrollBar.Scroll += new System.Windows.Forms.ScrollEventHandler(this.GammaScrollBar_Scroll);
            // 
            // PreviewPictureBox
            // 
            this.PreviewPictureBox.Location = new System.Drawing.Point(148, 12);
            this.PreviewPictureBox.Name = "PreviewPictureBox";
            this.PreviewPictureBox.Size = new System.Drawing.Size(300, 300);
            this.PreviewPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PreviewPictureBox.TabIndex = 3;
            this.PreviewPictureBox.TabStop = false;
            // 
            // BrightnessLabel
            // 
            this.BrightnessLabel.AutoSize = true;
            this.BrightnessLabel.Location = new System.Drawing.Point(89, 365);
            this.BrightnessLabel.Name = "BrightnessLabel";
            this.BrightnessLabel.Size = new System.Drawing.Size(56, 13);
            this.BrightnessLabel.TabIndex = 4;
            this.BrightnessLabel.Text = "Brightness";
            // 
            // ContrastLabel
            // 
            this.ContrastLabel.AutoSize = true;
            this.ContrastLabel.Location = new System.Drawing.Point(99, 399);
            this.ContrastLabel.Name = "ContrastLabel";
            this.ContrastLabel.Size = new System.Drawing.Size(46, 13);
            this.ContrastLabel.TabIndex = 5;
            this.ContrastLabel.Text = "Contrast";
            // 
            // GammaLabel
            // 
            this.GammaLabel.AutoSize = true;
            this.GammaLabel.Location = new System.Drawing.Point(102, 430);
            this.GammaLabel.Name = "GammaLabel";
            this.GammaLabel.Size = new System.Drawing.Size(43, 13);
            this.GammaLabel.TabIndex = 6;
            this.GammaLabel.Text = "Gamma";
            // 
            // BrightnessValue
            // 
            this.BrightnessValue.AutoSize = true;
            this.BrightnessValue.Location = new System.Drawing.Point(413, 365);
            this.BrightnessValue.Name = "BrightnessValue";
            this.BrightnessValue.Size = new System.Drawing.Size(13, 13);
            this.BrightnessValue.TabIndex = 7;
            this.BrightnessValue.Text = "0";
            // 
            // ContrastValue
            // 
            this.ContrastValue.AutoSize = true;
            this.ContrastValue.Location = new System.Drawing.Point(413, 398);
            this.ContrastValue.Name = "ContrastValue";
            this.ContrastValue.Size = new System.Drawing.Size(13, 13);
            this.ContrastValue.TabIndex = 8;
            this.ContrastValue.Text = "1";
            // 
            // GammaValue
            // 
            this.GammaValue.AutoSize = true;
            this.GammaValue.Location = new System.Drawing.Point(413, 430);
            this.GammaValue.Name = "GammaValue";
            this.GammaValue.Size = new System.Drawing.Size(13, 13);
            this.GammaValue.TabIndex = 9;
            this.GammaValue.Text = "1";
            // 
            // okButton
            // 
            this.okButton.Location = new System.Drawing.Point(537, 394);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(75, 23);
            this.okButton.TabIndex = 10;
            this.okButton.Text = "OK";
            this.okButton.UseVisualStyleBackColor = true;
            this.okButton.Click += new System.EventHandler(this.okButton_Click);
            // 
            // resetButton
            // 
            this.resetButton.Location = new System.Drawing.Point(537, 433);
            this.resetButton.Name = "resetButton";
            this.resetButton.Size = new System.Drawing.Size(75, 23);
            this.resetButton.TabIndex = 11;
            this.resetButton.Text = "Reset";
            this.resetButton.UseVisualStyleBackColor = true;
            this.resetButton.Click += new System.EventHandler(this.resetButton_Click);
            // 
            // CorrectionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(624, 468);
            this.Controls.Add(this.resetButton);
            this.Controls.Add(this.okButton);
            this.Controls.Add(this.GammaValue);
            this.Controls.Add(this.ContrastValue);
            this.Controls.Add(this.BrightnessValue);
            this.Controls.Add(this.GammaLabel);
            this.Controls.Add(this.ContrastLabel);
            this.Controls.Add(this.BrightnessLabel);
            this.Controls.Add(this.PreviewPictureBox);
            this.Controls.Add(this.GammaScrollBar);
            this.Controls.Add(this.ContrastScrollBar);
            this.Controls.Add(this.BrightnessScrollBar);
            this.Name = "CorrectionForm";
            this.Text = "CorrectionForm";
            ((System.ComponentModel.ISupportInitialize)(this.PreviewPictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.HScrollBar BrightnessScrollBar;
        private System.Windows.Forms.HScrollBar ContrastScrollBar;
        private System.Windows.Forms.HScrollBar GammaScrollBar;
        private System.Windows.Forms.PictureBox PreviewPictureBox;
        private System.Windows.Forms.Label BrightnessLabel;
        private System.Windows.Forms.Label ContrastLabel;
        private System.Windows.Forms.Label GammaLabel;
        private System.Windows.Forms.Label BrightnessValue;
        private System.Windows.Forms.Label ContrastValue;
        private System.Windows.Forms.Label GammaValue;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Button resetButton;
    }
}