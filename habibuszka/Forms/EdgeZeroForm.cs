﻿using habibuszka.Exceptions;
using habibuszka.Transformations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace habibuszka
{
    public partial class EdgeZeroForm : Form
    {
        private Image image;
        public Image modifiedImage;
        public EdgeZeroForm(Image image)
        {
            this.image = image;
            InitializeComponent();
        }

        private void OkButton_Click(object sender, EventArgs e)
        {
            try
            {
                int maskSize = ((MaskComboBox.SelectedIndex + 1) * 2) + 1;
                double sigma = double.Parse(SigmaTextBox.Text);
                int threshold = int.Parse(ThresholdTextBox.Text);
                EdgeZero edge = new EdgeZero(new Bitmap(image));
                modifiedImage = edge.Transform(maskSize, sigma, threshold);
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            catch
            {
                new HabibuszkaMainException("Podaj prawidłowe wartości.");
            }
        }
    }
}
