﻿namespace habibuszka
{
    partial class NoiseBilateralForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SigmaDLabel = new System.Windows.Forms.Label();
            this.SigmaRLabel = new System.Windows.Forms.Label();
            this.SigmaDTextBox = new System.Windows.Forms.TextBox();
            this.SigmaRTextBox = new System.Windows.Forms.TextBox();
            this.OkButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // SigmaDLabel
            // 
            this.SigmaDLabel.AutoSize = true;
            this.SigmaDLabel.Location = new System.Drawing.Point(34, 21);
            this.SigmaDLabel.Name = "SigmaDLabel";
            this.SigmaDLabel.Size = new System.Drawing.Size(45, 13);
            this.SigmaDLabel.TabIndex = 0;
            this.SigmaDLabel.Text = "sigma D";
            // 
            // SigmaRLabel
            // 
            this.SigmaRLabel.AutoSize = true;
            this.SigmaRLabel.Location = new System.Drawing.Point(34, 63);
            this.SigmaRLabel.Name = "SigmaRLabel";
            this.SigmaRLabel.Size = new System.Drawing.Size(45, 13);
            this.SigmaRLabel.TabIndex = 1;
            this.SigmaRLabel.Text = "sigma R";
            // 
            // SigmaDTextBox
            // 
            this.SigmaDTextBox.Location = new System.Drawing.Point(85, 18);
            this.SigmaDTextBox.Name = "SigmaDTextBox";
            this.SigmaDTextBox.Size = new System.Drawing.Size(100, 20);
            this.SigmaDTextBox.TabIndex = 2;
            // 
            // SigmaRTextBox
            // 
            this.SigmaRTextBox.Location = new System.Drawing.Point(85, 60);
            this.SigmaRTextBox.Name = "SigmaRTextBox";
            this.SigmaRTextBox.Size = new System.Drawing.Size(100, 20);
            this.SigmaRTextBox.TabIndex = 3;
            // 
            // OkButton
            // 
            this.OkButton.Location = new System.Drawing.Point(187, 121);
            this.OkButton.Name = "OkButton";
            this.OkButton.Size = new System.Drawing.Size(75, 23);
            this.OkButton.TabIndex = 4;
            this.OkButton.Text = "OK";
            this.OkButton.UseVisualStyleBackColor = true;
            this.OkButton.Click += new System.EventHandler(this.OkButton_Click);
            // 
            // NoiseBilateralForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(274, 156);
            this.Controls.Add(this.OkButton);
            this.Controls.Add(this.SigmaRTextBox);
            this.Controls.Add(this.SigmaDTextBox);
            this.Controls.Add(this.SigmaRLabel);
            this.Controls.Add(this.SigmaDLabel);
            this.Name = "NoiseBilateralForm";
            this.Text = "NoiseBilateralForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label SigmaDLabel;
        private System.Windows.Forms.Label SigmaRLabel;
        private System.Windows.Forms.TextBox SigmaDTextBox;
        private System.Windows.Forms.TextBox SigmaRTextBox;
        private System.Windows.Forms.Button OkButton;
    }
}