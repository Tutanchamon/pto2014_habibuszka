﻿using System;
namespace habibuszka
{
    partial class MapHorizonForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.AlphaTextBox = new System.Windows.Forms.TextBox();
            this.AlphaLabel = new System.Windows.Forms.Label();
            this.OkButton = new System.Windows.Forms.Button();
            this.ScaleTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.DirectionComboBox = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // AlphaTextBox
            // 
            this.AlphaTextBox.Location = new System.Drawing.Point(77, 24);
            this.AlphaTextBox.Name = "AlphaTextBox";
            this.AlphaTextBox.Size = new System.Drawing.Size(100, 20);
            this.AlphaTextBox.TabIndex = 5;
            // 
            // AlphaLabel
            // 
            this.AlphaLabel.Location = new System.Drawing.Point(38, 27);
            this.AlphaLabel.Name = "AlphaLabel";
            this.AlphaLabel.Size = new System.Drawing.Size(54, 13);
            this.AlphaLabel.TabIndex = 4;
            this.AlphaLabel.Text = "Alpha";
            // 
            // OkButton
            // 
            this.OkButton.Location = new System.Drawing.Point(197, 143);
            this.OkButton.Name = "OkButton";
            this.OkButton.Size = new System.Drawing.Size(75, 23);
            this.OkButton.TabIndex = 3;
            this.OkButton.Text = "OK";
            this.OkButton.UseVisualStyleBackColor = true;
            this.OkButton.Click += new System.EventHandler(this.OkButton_Click);
            // 
            // ScaleTextBox
            // 
            this.ScaleTextBox.Location = new System.Drawing.Point(77, 57);
            this.ScaleTextBox.Name = "ScaleTextBox";
            this.ScaleTextBox.Size = new System.Drawing.Size(100, 20);
            this.ScaleTextBox.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(37, 60);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Scale";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(22, 96);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Direction";
            // 
            // DirectionComboBox
            // 
            this.DirectionComboBox.DataSource = Enum.GetValues(typeof(habibuszka.Transformations.MapHorizon.Direction));
            this.DirectionComboBox.Location = new System.Drawing.Point(77, 93);
            this.DirectionComboBox.Name = "DirectionComboBox";
            this.DirectionComboBox.Size = new System.Drawing.Size(121, 21);
            this.DirectionComboBox.TabIndex = 8;
            // 
            // MapHorizonForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 178);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.DirectionComboBox);
            this.Controls.Add(this.ScaleTextBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.AlphaTextBox);
            this.Controls.Add(this.AlphaLabel);
            this.Controls.Add(this.OkButton);
            this.Name = "MapHorizonForm";
            this.Text = "MapHorizonForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox AlphaTextBox;
        private System.Windows.Forms.Label AlphaLabel;
        private System.Windows.Forms.Button OkButton;
        private System.Windows.Forms.TextBox ScaleTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox DirectionComboBox;
    }
}