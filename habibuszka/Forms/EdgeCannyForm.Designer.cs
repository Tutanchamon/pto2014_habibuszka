﻿namespace habibuszka
{
    partial class EdgeCannyForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.UpperThresholdTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.OkButton = new System.Windows.Forms.Button();
            this.LowerThresholdTextBox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // UpperThresholdTextBox
            // 
            this.UpperThresholdTextBox.Location = new System.Drawing.Point(105, 71);
            this.UpperThresholdTextBox.Name = "UpperThresholdTextBox";
            this.UpperThresholdTextBox.Size = new System.Drawing.Size(100, 20);
            this.UpperThresholdTextBox.TabIndex = 16;
            this.UpperThresholdTextBox.Text = "100";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(17, 74);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 13);
            this.label3.TabIndex = 15;
            this.label3.Text = "Upper threshold";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(17, 32);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 13);
            this.label2.TabIndex = 14;
            this.label2.Text = "Lower threshold";
            // 
            // OkButton
            // 
            this.OkButton.Location = new System.Drawing.Point(171, 126);
            this.OkButton.Name = "OkButton";
            this.OkButton.Size = new System.Drawing.Size(95, 23);
            this.OkButton.TabIndex = 13;
            this.OkButton.Text = "OK";
            this.OkButton.UseVisualStyleBackColor = true;
            this.OkButton.Click += new System.EventHandler(this.OkButton_Click);
            // 
            // LowerThresholdTextBox
            // 
            this.LowerThresholdTextBox.Location = new System.Drawing.Point(105, 29);
            this.LowerThresholdTextBox.Name = "LowerThresholdTextBox";
            this.LowerThresholdTextBox.Size = new System.Drawing.Size(100, 20);
            this.LowerThresholdTextBox.TabIndex = 12;
            this.LowerThresholdTextBox.Text = "50";
            // 
            // EdgeCannyForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(278, 161);
            this.Controls.Add(this.UpperThresholdTextBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.OkButton);
            this.Controls.Add(this.LowerThresholdTextBox);
            this.Name = "EdgeCannyForm";
            this.Text = "EdgeCannyForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox UpperThresholdTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button OkButton;
        private System.Windows.Forms.TextBox LowerThresholdTextBox;
    }
}