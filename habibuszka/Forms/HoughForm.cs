﻿using habibuszka.Exceptions;
using habibuszka.Transformations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace habibuszka
{
    public partial class HoughForm : Form
    {
        private Image image;
        public Image modifiedImage;
        public HoughForm(Image image)
        {
            this.image = image;
            InitializeComponent();
        }

        private void OkButton_Click(object sender, EventArgs e)
        {
            try
            {
                int theta = int.Parse(ThetaTextBox.Text);
                Hough hough = new Hough(new Bitmap(image));
                modifiedImage = hough.Transform(theta, false);
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            catch
            {
                new HabibuszkaMainException("Podaj prawidłową wartość.");
            }
        }
    }
}
