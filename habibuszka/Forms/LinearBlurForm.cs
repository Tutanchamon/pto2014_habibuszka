﻿using habibuszka.Transformations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace habibuszka
{
    public partial class LinearBlurForm : Form
    {
        private Image image;
        public Image modifiedImage;

        public LinearBlurForm(Image image)
        {
            InitializeComponent();
            this.image = image;
        }

        private void btLoad_Click(object sender, System.EventArgs e)
        {
            try
            {
                System.Array l_Array = Array.CreateInstance(typeof(double), (int)MaskSizeComboBox.SelectedValue, (int)MaskSizeComboBox.SelectedValue);
                gridArray1.LoadDataSource(l_Array);
            }
            catch (Exception err)
            {
                SourceLibrary.Windows.Forms.ErrorDialog.Show(this, err, "Error");
            }
        }

        private void OkButton_Click(object sender, EventArgs e)
        {
            int size = (int) MaskSizeComboBox.SelectedValue;
            bool normalize = normalizeCheckBox.Checked;
            double[,] matrix = new double[size + 1, size + 1];
            matrix = (double[,])gridArray1.Array;
            double[,] newMatrix = LinearBlur.GetMask(matrix, normalize);
            Convolution c = new Convolution(new Bitmap(image));
            Bitmap img = c.Convolute(newMatrix, Mode.RepeatEdge);
            modifiedImage = (Image)img;

            this.DialogResult = DialogResult.OK;
            this.Close();
        }
    }
}
