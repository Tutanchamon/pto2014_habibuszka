﻿using habibuszka.Transformations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace habibuszka
{
    public partial class EdgeLaplacianForm : Form
    {
        private Image image;
        public Image modifiedImage;
        public EdgeLaplacianForm(Image image)
        {
            this.image = image;
            InitializeComponent();
        }

        private void OkButton_Click(object sender, EventArgs e)
        {
            int maskSize = ((SizeComboBox.SelectedIndex + 1) * 2) + 1;
            EdgeLaplacian edge = new EdgeLaplacian(new Bitmap(image));
            modifiedImage = edge.Convolute(edge.GetMask(maskSize));
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
    }
}
