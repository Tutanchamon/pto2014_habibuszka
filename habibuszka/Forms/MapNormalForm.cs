﻿using habibuszka.Exceptions;
using habibuszka.Transformations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace habibuszka
{
    public partial class MapNormalForm : Form
    {
        private Image image;
        public Image modifiedImage;
        public MapNormalForm(Image image)
        {
            InitializeComponent();
            this.image = image;
        }

        private void OkButton_Click(object sender, EventArgs e)
        {
            try
            {
                double val = double.Parse(StrengthTextBox.Text);
                MapNormal map = new MapNormal(new Bitmap(image));
                Bitmap bmp = map.Transform(val);
                modifiedImage = (Image)bmp;
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            catch
            {
                new HabibuszkaMainException("Podaj prawidłową wartość");
            }
        }
    }
}
