﻿namespace habibuszka
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.mainMenu = new System.Windows.Forms.MenuStrip();
            this.FileMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.OpenFileMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.SaveBinaryMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.SaveASCIIMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.CloseAppMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.undoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.redoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.NegativeBtn = new System.Windows.Forms.Button();
            this.GrayscaleBtn = new System.Windows.Forms.Button();
            this.CorrectionBtn = new System.Windows.Forms.Button();
            this.CornerDetectionBtn = new System.Windows.Forms.Button();
            this.ZoomScrollBar = new System.Windows.Forms.HScrollBar();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.ZoomLabel = new System.Windows.Forms.Label();
            this.histogramSplitButton = new wyDay.Controls.SplitButton();
            this.HistogramContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.HistogramStretchingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.HistogramEqualizationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.BlurSplitButton = new wyDay.Controls.SplitButton();
            this.BlurContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.uniformBlurToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gaussianBlurToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.linearBlurToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.BWSplitButton = new wyDay.Controls.SplitButton();
            this.BWContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.manualBinToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gradientBinToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.NoiseReductionSplitButton = new wyDay.Controls.SplitButton();
            this.NoiseReductionContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.medianNoiseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bilateralNoiseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.MorphologicalContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.dilateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.erodeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.MorphologicalSplitButton = new wyDay.Controls.SplitButton();
            this.EdgeDetectionContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.sobelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.prewittToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.robertsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.laplacianToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.zerocrossingLoGToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cannyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.EdgeDetectionSplitButton = new wyDay.Controls.SplitButton();
            this.TexturesContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.heightMapToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.normalMapToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.horizonMapToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.TexturesSplitButton = new wyDay.Controls.SplitButton();
            this.TransformationsContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.houghToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.houghLinesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.TransformationsSplitButton = new wyDay.Controls.SplitButton();
            this.houghRectanglesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.segmentationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mainMenu.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            this.HistogramContextMenu.SuspendLayout();
            this.BlurContextMenu.SuspendLayout();
            this.BWContextMenu.SuspendLayout();
            this.NoiseReductionContextMenu.SuspendLayout();
            this.MorphologicalContextMenu.SuspendLayout();
            this.EdgeDetectionContextMenu.SuspendLayout();
            this.TexturesContextMenu.SuspendLayout();
            this.TransformationsContextMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainMenu
            // 
            this.mainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.FileMenuItem,
            this.editToolStripMenuItem});
            this.mainMenu.Location = new System.Drawing.Point(0, 0);
            this.mainMenu.Name = "mainMenu";
            this.mainMenu.Size = new System.Drawing.Size(814, 24);
            this.mainMenu.TabIndex = 0;
            this.mainMenu.Text = "menuStrip1";
            // 
            // FileMenuItem
            // 
            this.FileMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.OpenFileMenuItem,
            this.toolStripSeparator1,
            this.SaveBinaryMenuItem,
            this.SaveASCIIMenuItem,
            this.toolStripSeparator2,
            this.CloseAppMenuItem});
            this.FileMenuItem.Name = "FileMenuItem";
            this.FileMenuItem.Size = new System.Drawing.Size(37, 20);
            this.FileMenuItem.Text = "File";
            this.FileMenuItem.Click += new System.EventHandler(this.FileMenuItem_Click);
            // 
            // OpenFileMenuItem
            // 
            this.OpenFileMenuItem.Name = "OpenFileMenuItem";
            this.OpenFileMenuItem.Size = new System.Drawing.Size(157, 22);
            this.OpenFileMenuItem.Text = "Open image";
            this.OpenFileMenuItem.Click += new System.EventHandler(this.openFileMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(154, 6);
            // 
            // SaveBinaryMenuItem
            // 
            this.SaveBinaryMenuItem.Name = "SaveBinaryMenuItem";
            this.SaveBinaryMenuItem.Size = new System.Drawing.Size(157, 22);
            this.SaveBinaryMenuItem.Text = "Save binary as...";
            this.SaveBinaryMenuItem.Click += new System.EventHandler(this.SaveBinaryMenuItem_Click);
            // 
            // SaveASCIIMenuItem
            // 
            this.SaveASCIIMenuItem.Name = "SaveASCIIMenuItem";
            this.SaveASCIIMenuItem.Size = new System.Drawing.Size(157, 22);
            this.SaveASCIIMenuItem.Text = "Save ASCII as...";
            this.SaveASCIIMenuItem.Click += new System.EventHandler(this.SaveASCIIMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(154, 6);
            // 
            // CloseAppMenuItem
            // 
            this.CloseAppMenuItem.Name = "CloseAppMenuItem";
            this.CloseAppMenuItem.Size = new System.Drawing.Size(157, 22);
            this.CloseAppMenuItem.Text = "Close";
            this.CloseAppMenuItem.Click += new System.EventHandler(this.closeAppMenuItem_Click);
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.undoToolStripMenuItem,
            this.redoToolStripMenuItem});
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
            this.editToolStripMenuItem.Text = "Edit";
            // 
            // undoToolStripMenuItem
            // 
            this.undoToolStripMenuItem.Enabled = false;
            this.undoToolStripMenuItem.Name = "undoToolStripMenuItem";
            this.undoToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Z)));
            this.undoToolStripMenuItem.Size = new System.Drawing.Size(144, 22);
            this.undoToolStripMenuItem.Text = "Undo";
            this.undoToolStripMenuItem.Click += new System.EventHandler(this.undoToolStripMenuItem_Click);
            // 
            // redoToolStripMenuItem
            // 
            this.redoToolStripMenuItem.Enabled = false;
            this.redoToolStripMenuItem.Name = "redoToolStripMenuItem";
            this.redoToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Y)));
            this.redoToolStripMenuItem.Size = new System.Drawing.Size(144, 22);
            this.redoToolStripMenuItem.Text = "Redo";
            this.redoToolStripMenuItem.Click += new System.EventHandler(this.redoToolStripMenuItem_Click);
            // 
            // NegativeBtn
            // 
            this.NegativeBtn.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.NegativeBtn.Location = new System.Drawing.Point(0, 27);
            this.NegativeBtn.Name = "NegativeBtn";
            this.NegativeBtn.Size = new System.Drawing.Size(160, 45);
            this.NegativeBtn.TabIndex = 2;
            this.NegativeBtn.Text = "Negative";
            this.NegativeBtn.UseVisualStyleBackColor = true;
            this.NegativeBtn.Click += new System.EventHandler(this.NegativeBtn_Click);
            // 
            // GrayscaleBtn
            // 
            this.GrayscaleBtn.Location = new System.Drawing.Point(0, 70);
            this.GrayscaleBtn.Name = "GrayscaleBtn";
            this.GrayscaleBtn.Size = new System.Drawing.Size(160, 45);
            this.GrayscaleBtn.TabIndex = 3;
            this.GrayscaleBtn.Text = "Grayscale";
            this.GrayscaleBtn.UseVisualStyleBackColor = true;
            this.GrayscaleBtn.Click += new System.EventHandler(this.GrayscaleBtn_Click);
            // 
            // CorrectionBtn
            // 
            this.CorrectionBtn.Location = new System.Drawing.Point(0, 113);
            this.CorrectionBtn.Name = "CorrectionBtn";
            this.CorrectionBtn.Size = new System.Drawing.Size(160, 45);
            this.CorrectionBtn.TabIndex = 4;
            this.CorrectionBtn.Text = "Correction";
            this.CorrectionBtn.UseVisualStyleBackColor = true;
            this.CorrectionBtn.Click += new System.EventHandler(this.CorrectionBtn_Click);
            // 
            // CornerDetectionBtn
            // 
            this.CornerDetectionBtn.Location = new System.Drawing.Point(0, 500);
            this.CornerDetectionBtn.Name = "CornerDetectionBtn";
            this.CornerDetectionBtn.Size = new System.Drawing.Size(160, 45);
            this.CornerDetectionBtn.TabIndex = 13;
            this.CornerDetectionBtn.Text = "Corner detection";
            this.CornerDetectionBtn.UseVisualStyleBackColor = true;
            this.CornerDetectionBtn.Click += new System.EventHandler(this.CornerDetectionBtn_Click);
            // 
            // ZoomScrollBar
            // 
            this.ZoomScrollBar.Location = new System.Drawing.Point(9, 592);
            this.ZoomScrollBar.Maximum = 400;
            this.ZoomScrollBar.Minimum = -99;
            this.ZoomScrollBar.Name = "ZoomScrollBar";
            this.ZoomScrollBar.Size = new System.Drawing.Size(151, 17);
            this.ZoomScrollBar.TabIndex = 14;
            this.ZoomScrollBar.Scroll += new System.Windows.Forms.ScrollEventHandler(this.ZoomScrollBar_Scroll);
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.Controls.Add(this.pictureBox);
            this.panel1.Location = new System.Drawing.Point(166, 27);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(500, 500);
            this.panel1.TabIndex = 15;
            // 
            // pictureBox
            // 
            this.pictureBox.Location = new System.Drawing.Point(3, 3);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(300, 300);
            this.pictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox.TabIndex = 1;
            this.pictureBox.TabStop = false;
            // 
            // ZoomLabel
            // 
            this.ZoomLabel.AutoSize = true;
            this.ZoomLabel.Location = new System.Drawing.Point(166, 596);
            this.ZoomLabel.Name = "ZoomLabel";
            this.ZoomLabel.Size = new System.Drawing.Size(33, 13);
            this.ZoomLabel.TabIndex = 16;
            this.ZoomLabel.Text = "100%";
            // 
            // histogramSplitButton
            // 
            this.histogramSplitButton.AutoSize = true;
            this.histogramSplitButton.ContextMenuStrip = this.HistogramContextMenu;
            this.histogramSplitButton.Location = new System.Drawing.Point(0, 156);
            this.histogramSplitButton.Name = "histogramSplitButton";
            this.histogramSplitButton.Size = new System.Drawing.Size(160, 45);
            this.histogramSplitButton.SplitMenuStrip = this.HistogramContextMenu;
            this.histogramSplitButton.TabIndex = 18;
            this.histogramSplitButton.Text = "Histogram";
            this.histogramSplitButton.UseVisualStyleBackColor = true;
            this.histogramSplitButton.Click += new System.EventHandler(this.histogramSplitButton_Click);
            // 
            // HistogramContextMenu
            // 
            this.HistogramContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.HistogramStretchingToolStripMenuItem,
            this.HistogramEqualizationToolStripMenuItem});
            this.HistogramContextMenu.Name = "HistogramContextMenu";
            this.HistogramContextMenu.Size = new System.Drawing.Size(198, 48);
            // 
            // HistogramStretchingToolStripMenuItem
            // 
            this.HistogramStretchingToolStripMenuItem.Name = "HistogramStretchingToolStripMenuItem";
            this.HistogramStretchingToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.HistogramStretchingToolStripMenuItem.Text = "Histogram Stretching";
            this.HistogramStretchingToolStripMenuItem.Click += new System.EventHandler(this.HistogramStretchingToolStripMenuItem_Click);
            // 
            // HistogramEqualizationToolStripMenuItem
            // 
            this.HistogramEqualizationToolStripMenuItem.Name = "HistogramEqualizationToolStripMenuItem";
            this.HistogramEqualizationToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.HistogramEqualizationToolStripMenuItem.Text = "Histogram Equalization";
            this.HistogramEqualizationToolStripMenuItem.Click += new System.EventHandler(this.HistogramEqualizationToolStripMenuItem_Click);
            // 
            // BlurSplitButton
            // 
            this.BlurSplitButton.AutoSize = true;
            this.BlurSplitButton.ContextMenuStrip = this.BlurContextMenu;
            this.BlurSplitButton.Location = new System.Drawing.Point(0, 199);
            this.BlurSplitButton.Name = "BlurSplitButton";
            this.BlurSplitButton.Size = new System.Drawing.Size(160, 45);
            this.BlurSplitButton.SplitMenuStrip = this.BlurContextMenu;
            this.BlurSplitButton.TabIndex = 19;
            this.BlurSplitButton.Text = "Blur";
            this.BlurSplitButton.UseVisualStyleBackColor = true;
            // 
            // BlurContextMenu
            // 
            this.BlurContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.uniformBlurToolStripMenuItem,
            this.gaussianBlurToolStripMenuItem,
            this.linearBlurToolStripMenuItem});
            this.BlurContextMenu.Name = "BlurContextMenu";
            this.BlurContextMenu.Size = new System.Drawing.Size(146, 70);
            // 
            // uniformBlurToolStripMenuItem
            // 
            this.uniformBlurToolStripMenuItem.Name = "uniformBlurToolStripMenuItem";
            this.uniformBlurToolStripMenuItem.Size = new System.Drawing.Size(145, 22);
            this.uniformBlurToolStripMenuItem.Text = "Uniform blur";
            this.uniformBlurToolStripMenuItem.Click += new System.EventHandler(this.uniformBlurToolStripMenuItem_Click);
            // 
            // gaussianBlurToolStripMenuItem
            // 
            this.gaussianBlurToolStripMenuItem.Name = "gaussianBlurToolStripMenuItem";
            this.gaussianBlurToolStripMenuItem.Size = new System.Drawing.Size(145, 22);
            this.gaussianBlurToolStripMenuItem.Text = "Gaussian blur";
            this.gaussianBlurToolStripMenuItem.Click += new System.EventHandler(this.gaussianBlurToolStripMenuItem_Click);
            // 
            // linearBlurToolStripMenuItem
            // 
            this.linearBlurToolStripMenuItem.Name = "linearBlurToolStripMenuItem";
            this.linearBlurToolStripMenuItem.Size = new System.Drawing.Size(145, 22);
            this.linearBlurToolStripMenuItem.Text = "Linear blur";
            this.linearBlurToolStripMenuItem.Click += new System.EventHandler(this.linearBlurToolStripMenuItem_Click);
            // 
            // BWSplitButton
            // 
            this.BWSplitButton.AutoSize = true;
            this.BWSplitButton.ContextMenuStrip = this.BWContextMenu;
            this.BWSplitButton.Location = new System.Drawing.Point(0, 242);
            this.BWSplitButton.Name = "BWSplitButton";
            this.BWSplitButton.Size = new System.Drawing.Size(160, 45);
            this.BWSplitButton.SplitMenuStrip = this.BWContextMenu;
            this.BWSplitButton.TabIndex = 20;
            this.BWSplitButton.Text = "Black and white";
            this.BWSplitButton.UseVisualStyleBackColor = true;
            // 
            // BWContextMenu
            // 
            this.BWContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.manualBinToolStripMenuItem,
            this.gradientBinToolStripMenuItem});
            this.BWContextMenu.Name = "BWContextMenu";
            this.BWContextMenu.Size = new System.Drawing.Size(185, 48);
            // 
            // manualBinToolStripMenuItem
            // 
            this.manualBinToolStripMenuItem.Name = "manualBinToolStripMenuItem";
            this.manualBinToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.manualBinToolStripMenuItem.Text = "Manual binarization";
            this.manualBinToolStripMenuItem.Click += new System.EventHandler(this.manualBinToolStripMenuItem_Click);
            // 
            // gradientBinToolStripMenuItem
            // 
            this.gradientBinToolStripMenuItem.Name = "gradientBinToolStripMenuItem";
            this.gradientBinToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.gradientBinToolStripMenuItem.Text = "Gradient binarization";
            this.gradientBinToolStripMenuItem.Click += new System.EventHandler(this.gradientBinToolStripMenuItem_Click);
            // 
            // NoiseReductionSplitButton
            // 
            this.NoiseReductionSplitButton.AutoSize = true;
            this.NoiseReductionSplitButton.ContextMenuStrip = this.NoiseReductionContextMenu;
            this.NoiseReductionSplitButton.Location = new System.Drawing.Point(0, 285);
            this.NoiseReductionSplitButton.Name = "NoiseReductionSplitButton";
            this.NoiseReductionSplitButton.Size = new System.Drawing.Size(160, 45);
            this.NoiseReductionSplitButton.SplitMenuStrip = this.NoiseReductionContextMenu;
            this.NoiseReductionSplitButton.TabIndex = 21;
            this.NoiseReductionSplitButton.Text = "Noise reduction";
            this.NoiseReductionSplitButton.UseVisualStyleBackColor = true;
            // 
            // NoiseReductionContextMenu
            // 
            this.NoiseReductionContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.medianNoiseToolStripMenuItem,
            this.bilateralNoiseToolStripMenuItem});
            this.NoiseReductionContextMenu.Name = "NoiseReductionContextMenu";
            this.NoiseReductionContextMenu.Size = new System.Drawing.Size(148, 48);
            // 
            // medianNoiseToolStripMenuItem
            // 
            this.medianNoiseToolStripMenuItem.Name = "medianNoiseToolStripMenuItem";
            this.medianNoiseToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
            this.medianNoiseToolStripMenuItem.Text = "Median noise";
            this.medianNoiseToolStripMenuItem.Click += new System.EventHandler(this.medianNoiseToolStripMenuItem_Click);
            // 
            // bilateralNoiseToolStripMenuItem
            // 
            this.bilateralNoiseToolStripMenuItem.Name = "bilateralNoiseToolStripMenuItem";
            this.bilateralNoiseToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
            this.bilateralNoiseToolStripMenuItem.Text = "Bilateral noise";
            this.bilateralNoiseToolStripMenuItem.Click += new System.EventHandler(this.bilateralNoiseToolStripMenuItem_Click);
            // 
            // MorphologicalContextMenu
            // 
            this.MorphologicalContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dilateToolStripMenuItem,
            this.erodeToolStripMenuItem,
            this.openToolStripMenuItem,
            this.closeToolStripMenuItem});
            this.MorphologicalContextMenu.Name = "MorphologicalContextMenu";
            this.MorphologicalContextMenu.Size = new System.Drawing.Size(105, 92);
            // 
            // dilateToolStripMenuItem
            // 
            this.dilateToolStripMenuItem.Name = "dilateToolStripMenuItem";
            this.dilateToolStripMenuItem.Size = new System.Drawing.Size(104, 22);
            this.dilateToolStripMenuItem.Text = "Dilate";
            this.dilateToolStripMenuItem.Click += new System.EventHandler(this.dilateToolStripMenuItem_Click);
            // 
            // erodeToolStripMenuItem
            // 
            this.erodeToolStripMenuItem.Name = "erodeToolStripMenuItem";
            this.erodeToolStripMenuItem.Size = new System.Drawing.Size(104, 22);
            this.erodeToolStripMenuItem.Text = "Erode";
            this.erodeToolStripMenuItem.Click += new System.EventHandler(this.erodeToolStripMenuItem_Click);
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(104, 22);
            this.openToolStripMenuItem.Text = "Open";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // closeToolStripMenuItem
            // 
            this.closeToolStripMenuItem.Name = "closeToolStripMenuItem";
            this.closeToolStripMenuItem.Size = new System.Drawing.Size(104, 22);
            this.closeToolStripMenuItem.Text = "Close";
            this.closeToolStripMenuItem.Click += new System.EventHandler(this.closeToolStripMenuItem_Click);
            // 
            // MorphologicalSplitButton
            // 
            this.MorphologicalSplitButton.AutoSize = true;
            this.MorphologicalSplitButton.ContextMenuStrip = this.MorphologicalContextMenu;
            this.MorphologicalSplitButton.Location = new System.Drawing.Point(0, 328);
            this.MorphologicalSplitButton.Name = "MorphologicalSplitButton";
            this.MorphologicalSplitButton.Size = new System.Drawing.Size(160, 45);
            this.MorphologicalSplitButton.SplitMenuStrip = this.MorphologicalContextMenu;
            this.MorphologicalSplitButton.TabIndex = 23;
            this.MorphologicalSplitButton.Text = "Morphological";
            this.MorphologicalSplitButton.UseVisualStyleBackColor = true;
            // 
            // EdgeDetectionContextMenu
            // 
            this.EdgeDetectionContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sobelToolStripMenuItem,
            this.prewittToolStripMenuItem,
            this.robertsToolStripMenuItem,
            this.laplacianToolStripMenuItem,
            this.zerocrossingLoGToolStripMenuItem,
            this.cannyToolStripMenuItem});
            this.EdgeDetectionContextMenu.Name = "EdgeDetectionContextMenu";
            this.EdgeDetectionContextMenu.Size = new System.Drawing.Size(180, 136);
            // 
            // sobelToolStripMenuItem
            // 
            this.sobelToolStripMenuItem.Name = "sobelToolStripMenuItem";
            this.sobelToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this.sobelToolStripMenuItem.Text = "Sobel";
            this.sobelToolStripMenuItem.Click += new System.EventHandler(this.sobelToolStripMenuItem_Click);
            // 
            // prewittToolStripMenuItem
            // 
            this.prewittToolStripMenuItem.Name = "prewittToolStripMenuItem";
            this.prewittToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this.prewittToolStripMenuItem.Text = "Prewitt";
            this.prewittToolStripMenuItem.Click += new System.EventHandler(this.prewittToolStripMenuItem_Click);
            // 
            // robertsToolStripMenuItem
            // 
            this.robertsToolStripMenuItem.Name = "robertsToolStripMenuItem";
            this.robertsToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this.robertsToolStripMenuItem.Text = "Roberts";
            this.robertsToolStripMenuItem.Click += new System.EventHandler(this.robertsToolStripMenuItem_Click);
            // 
            // laplacianToolStripMenuItem
            // 
            this.laplacianToolStripMenuItem.Name = "laplacianToolStripMenuItem";
            this.laplacianToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this.laplacianToolStripMenuItem.Text = "Laplacian";
            this.laplacianToolStripMenuItem.Click += new System.EventHandler(this.laplacianToolStripMenuItem_Click);
            // 
            // zerocrossingLoGToolStripMenuItem
            // 
            this.zerocrossingLoGToolStripMenuItem.Name = "zerocrossingLoGToolStripMenuItem";
            this.zerocrossingLoGToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this.zerocrossingLoGToolStripMenuItem.Text = "Zero-crossing (LoG)";
            this.zerocrossingLoGToolStripMenuItem.Click += new System.EventHandler(this.zerocrossingLoGToolStripMenuItem_Click);
            // 
            // cannyToolStripMenuItem
            // 
            this.cannyToolStripMenuItem.Name = "cannyToolStripMenuItem";
            this.cannyToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this.cannyToolStripMenuItem.Text = "Canny";
            this.cannyToolStripMenuItem.Click += new System.EventHandler(this.cannyToolStripMenuItem_Click);
            // 
            // EdgeDetectionSplitButton
            // 
            this.EdgeDetectionSplitButton.AutoSize = true;
            this.EdgeDetectionSplitButton.ContextMenuStrip = this.EdgeDetectionContextMenu;
            this.EdgeDetectionSplitButton.Location = new System.Drawing.Point(0, 371);
            this.EdgeDetectionSplitButton.Name = "EdgeDetectionSplitButton";
            this.EdgeDetectionSplitButton.Size = new System.Drawing.Size(160, 45);
            this.EdgeDetectionSplitButton.SplitMenuStrip = this.EdgeDetectionContextMenu;
            this.EdgeDetectionSplitButton.TabIndex = 25;
            this.EdgeDetectionSplitButton.Text = "Edge detection";
            this.EdgeDetectionSplitButton.UseVisualStyleBackColor = true;
            // 
            // TexturesContextMenu
            // 
            this.TexturesContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.heightMapToolStripMenuItem,
            this.normalMapToolStripMenuItem,
            this.horizonMapToolStripMenuItem});
            this.TexturesContextMenu.Name = "TexturesContextMenu";
            this.TexturesContextMenu.Size = new System.Drawing.Size(144, 70);
            // 
            // heightMapToolStripMenuItem
            // 
            this.heightMapToolStripMenuItem.Name = "heightMapToolStripMenuItem";
            this.heightMapToolStripMenuItem.Size = new System.Drawing.Size(143, 22);
            this.heightMapToolStripMenuItem.Text = "Height map";
            this.heightMapToolStripMenuItem.Click += new System.EventHandler(this.heightMapToolStripMenuItem_Click);
            // 
            // normalMapToolStripMenuItem
            // 
            this.normalMapToolStripMenuItem.Name = "normalMapToolStripMenuItem";
            this.normalMapToolStripMenuItem.Size = new System.Drawing.Size(143, 22);
            this.normalMapToolStripMenuItem.Text = "Normal map";
            this.normalMapToolStripMenuItem.Click += new System.EventHandler(this.normalMapToolStripMenuItem_Click);
            // 
            // horizonMapToolStripMenuItem
            // 
            this.horizonMapToolStripMenuItem.Name = "horizonMapToolStripMenuItem";
            this.horizonMapToolStripMenuItem.Size = new System.Drawing.Size(143, 22);
            this.horizonMapToolStripMenuItem.Text = "Horizon map";
            this.horizonMapToolStripMenuItem.Click += new System.EventHandler(this.horizonMapToolStripMenuItem_Click);
            // 
            // TexturesSplitButton
            // 
            this.TexturesSplitButton.AutoSize = true;
            this.TexturesSplitButton.ContextMenuStrip = this.TexturesContextMenu;
            this.TexturesSplitButton.Location = new System.Drawing.Point(0, 414);
            this.TexturesSplitButton.Name = "TexturesSplitButton";
            this.TexturesSplitButton.Size = new System.Drawing.Size(160, 45);
            this.TexturesSplitButton.SplitMenuStrip = this.TexturesContextMenu;
            this.TexturesSplitButton.TabIndex = 27;
            this.TexturesSplitButton.Text = "Textures";
            this.TexturesSplitButton.UseVisualStyleBackColor = true;
            // 
            // TransformationsContextMenu
            // 
            this.TransformationsContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.houghToolStripMenuItem,
            this.houghLinesToolStripMenuItem,
            this.houghRectanglesToolStripMenuItem,
            this.segmentationToolStripMenuItem});
            this.TransformationsContextMenu.Name = "TransformationsContextMenu";
            this.TransformationsContextMenu.Size = new System.Drawing.Size(177, 114);
            // 
            // houghToolStripMenuItem
            // 
            this.houghToolStripMenuItem.Name = "houghToolStripMenuItem";
            this.houghToolStripMenuItem.Size = new System.Drawing.Size(176, 22);
            this.houghToolStripMenuItem.Text = "Hough";
            this.houghToolStripMenuItem.Click += new System.EventHandler(this.houghToolStripMenuItem_Click);
            // 
            // houghLinesToolStripMenuItem
            // 
            this.houghLinesToolStripMenuItem.Name = "houghLinesToolStripMenuItem";
            this.houghLinesToolStripMenuItem.Size = new System.Drawing.Size(176, 22);
            this.houghLinesToolStripMenuItem.Text = "Hough - lines";
            this.houghLinesToolStripMenuItem.Click += new System.EventHandler(this.houghLinesToolStripMenuItem_Click);
            // 
            // TransformationsSplitButton
            // 
            this.TransformationsSplitButton.AutoSize = true;
            this.TransformationsSplitButton.ContextMenuStrip = this.TransformationsContextMenu;
            this.TransformationsSplitButton.Location = new System.Drawing.Point(0, 457);
            this.TransformationsSplitButton.Name = "TransformationsSplitButton";
            this.TransformationsSplitButton.Size = new System.Drawing.Size(160, 45);
            this.TransformationsSplitButton.SplitMenuStrip = this.TransformationsContextMenu;
            this.TransformationsSplitButton.TabIndex = 29;
            this.TransformationsSplitButton.Text = "Transformations";
            this.TransformationsSplitButton.UseVisualStyleBackColor = true;
            // 
            // houghRectanglesToolStripMenuItem
            // 
            this.houghRectanglesToolStripMenuItem.Name = "houghRectanglesToolStripMenuItem";
            this.houghRectanglesToolStripMenuItem.Size = new System.Drawing.Size(176, 22);
            this.houghRectanglesToolStripMenuItem.Text = "Hough - rectangles";
            this.houghRectanglesToolStripMenuItem.Click += new System.EventHandler(this.houghRectanglesToolStripMenuItem_Click);
            // 
            // segmentationToolStripMenuItem
            // 
            this.segmentationToolStripMenuItem.Name = "segmentationToolStripMenuItem";
            this.segmentationToolStripMenuItem.Size = new System.Drawing.Size(176, 22);
            this.segmentationToolStripMenuItem.Text = "Segmentation";
            this.segmentationToolStripMenuItem.Click += new System.EventHandler(this.segmentationToolStripMenuItem_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(814, 618);
            this.Controls.Add(this.TransformationsSplitButton);
            this.Controls.Add(this.TexturesSplitButton);
            this.Controls.Add(this.EdgeDetectionSplitButton);
            this.Controls.Add(this.MorphologicalSplitButton);
            this.Controls.Add(this.NoiseReductionSplitButton);
            this.Controls.Add(this.BWSplitButton);
            this.Controls.Add(this.BlurSplitButton);
            this.Controls.Add(this.histogramSplitButton);
            this.Controls.Add(this.ZoomLabel);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.ZoomScrollBar);
            this.Controls.Add(this.CornerDetectionBtn);
            this.Controls.Add(this.CorrectionBtn);
            this.Controls.Add(this.GrayscaleBtn);
            this.Controls.Add(this.NegativeBtn);
            this.Controls.Add(this.mainMenu);
            this.MainMenuStrip = this.mainMenu;
            this.Name = "MainForm";
            this.Text = "Habibuszka";
            this.mainMenu.ResumeLayout(false);
            this.mainMenu.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            this.HistogramContextMenu.ResumeLayout(false);
            this.BlurContextMenu.ResumeLayout(false);
            this.BWContextMenu.ResumeLayout(false);
            this.NoiseReductionContextMenu.ResumeLayout(false);
            this.MorphologicalContextMenu.ResumeLayout(false);
            this.EdgeDetectionContextMenu.ResumeLayout(false);
            this.TexturesContextMenu.ResumeLayout(false);
            this.TransformationsContextMenu.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip mainMenu;
        private System.Windows.Forms.ToolStripMenuItem FileMenuItem;
        private System.Windows.Forms.ToolStripMenuItem OpenFileMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem CloseAppMenuItem;
        private System.Windows.Forms.PictureBox pictureBox;
        private System.Windows.Forms.Button NegativeBtn;
        private System.Windows.Forms.Button GrayscaleBtn;
        private System.Windows.Forms.Button CorrectionBtn;
        private System.Windows.Forms.Button CornerDetectionBtn;
        private System.Windows.Forms.ToolStripMenuItem SaveBinaryMenuItem;
        private System.Windows.Forms.ToolStripMenuItem SaveASCIIMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.HScrollBar ZoomScrollBar;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label ZoomLabel;
        private wyDay.Controls.SplitButton histogramSplitButton;
        private System.Windows.Forms.ContextMenuStrip HistogramContextMenu;
        private System.Windows.Forms.ToolStripMenuItem HistogramStretchingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem HistogramEqualizationToolStripMenuItem;
        private wyDay.Controls.SplitButton BlurSplitButton;
        private System.Windows.Forms.ContextMenuStrip BlurContextMenu;
        private System.Windows.Forms.ToolStripMenuItem uniformBlurToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gaussianBlurToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem linearBlurToolStripMenuItem;
        private wyDay.Controls.SplitButton BWSplitButton;
        private System.Windows.Forms.ContextMenuStrip BWContextMenu;
        private System.Windows.Forms.ToolStripMenuItem manualBinToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gradientBinToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem undoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem redoToolStripMenuItem;
        private wyDay.Controls.SplitButton NoiseReductionSplitButton;
        private System.Windows.Forms.ContextMenuStrip NoiseReductionContextMenu;
        private System.Windows.Forms.ToolStripMenuItem medianNoiseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bilateralNoiseToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip MorphologicalContextMenu;
        private System.Windows.Forms.ToolStripMenuItem dilateToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem erodeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem closeToolStripMenuItem;
        private wyDay.Controls.SplitButton MorphologicalSplitButton;
        private System.Windows.Forms.ContextMenuStrip EdgeDetectionContextMenu;
        private System.Windows.Forms.ToolStripMenuItem sobelToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem prewittToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem robertsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem laplacianToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem zerocrossingLoGToolStripMenuItem;
        private wyDay.Controls.SplitButton EdgeDetectionSplitButton;
        private System.Windows.Forms.ContextMenuStrip TexturesContextMenu;
        private System.Windows.Forms.ToolStripMenuItem heightMapToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem normalMapToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem horizonMapToolStripMenuItem;
        private wyDay.Controls.SplitButton TexturesSplitButton;
        private System.Windows.Forms.ToolStripMenuItem cannyToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip TransformationsContextMenu;
        private System.Windows.Forms.ToolStripMenuItem houghToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem houghLinesToolStripMenuItem;
        private wyDay.Controls.SplitButton TransformationsSplitButton;
        private System.Windows.Forms.ToolStripMenuItem houghRectanglesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem segmentationToolStripMenuItem;
    }
}