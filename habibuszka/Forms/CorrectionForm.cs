﻿using habibuszka.Correction;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace habibuszka
{
    public partial class CorrectionForm : Form
    {
        private List<Dictionary<string, int>> operations;
        public Image modifiedImage;
        private Image loadedImage;
        private Image previewImage;
        private BrightnessOperation brightnessOperation;
        private ContrastOperation contrastOperation;
        private GammaOperation gammaOperation;
        bool isBrightness;
        bool isContrast;
        bool isGamma;

        public CorrectionForm(Image image)
        {
            InitializeComponent();
            operations = new List<Dictionary<string, int>>();
            loadedImage = image;
            ChangePreviewResolution();
            PreviewPictureBox.Image = previewImage;
            brightnessOperation = new BrightnessOperation(PreviewPictureBox.Image);
            contrastOperation = new ContrastOperation(PreviewPictureBox.Image);
            gammaOperation = new GammaOperation(PreviewPictureBox.Image);
        }

        private void ChangePreviewResolution()
        {
            int previewSize = 100;
            int width = loadedImage.Width;
            int height = loadedImage.Height;
            Size newSize = new Size();
            if (width > previewSize || height > previewSize)
            {
                if (width > height)
                {
                    newSize.Width = previewSize;
                    newSize.Height = (int)(((double)newSize.Width / (double)width) * height);
                }
                else
                {
                    newSize.Height = previewSize;
                    newSize.Width = (int)(((double)newSize.Height / (double)height) * width);
                }
                previewImage = new Bitmap(loadedImage, newSize);
            }
            else
                previewImage = (Image)loadedImage.Clone();
        }

        private void BrightnessScrollBar_Scroll(object sender, ScrollEventArgs e)
        {
            if (isContrast)
                operations.Add(new Dictionary<string, int>() { { "contrast", ContrastScrollBar.Value } });
            if (isGamma)
                operations.Add(new Dictionary<string, int>() { { "gamma", GammaScrollBar.Value } });

            brightnessOperation.value = BrightnessScrollBar.Value;
            BrightnessValue.Text = BrightnessScrollBar.Value.ToString();
            PreviewPictureBox.Image = brightnessOperation.Execute();
            contrastOperation.UpdateImage(PreviewPictureBox.Image, ContrastScrollBar.Value);
            gammaOperation.UpdateImage(PreviewPictureBox.Image, GammaScrollBar.Value);
            isBrightness = true;
            isContrast = false;
            isGamma = false;
        }

        private void ContrastScrollBar_Scroll(object sender, ScrollEventArgs e)
        {
            if (isBrightness)
                operations.Add(new Dictionary<string, int>() { { "brightness", BrightnessScrollBar.Value } });
            if (isGamma)
                operations.Add(new Dictionary<string, int>() { { "gamma", GammaScrollBar.Value } });

            contrastOperation.value = ContrastScrollBar.Value;
            ContrastValue.Text = ((double)ContrastScrollBar.Value / 100.0).ToString();
            PreviewPictureBox.Image = contrastOperation.Execute();
            brightnessOperation.UpdateImage(PreviewPictureBox.Image, BrightnessScrollBar.Value);
            gammaOperation.UpdateImage(PreviewPictureBox.Image, GammaScrollBar.Value);
            isContrast = true;
            isBrightness = false;
            isGamma = false;
        }

        private void GammaScrollBar_Scroll(object sender, ScrollEventArgs e)
        {
            if (isBrightness)
                operations.Add(new Dictionary<string, int>() { { "brightness", BrightnessScrollBar.Value } });
            if (isContrast)
                operations.Add(new Dictionary<string, int>() { { "contrast", ContrastScrollBar.Value } });

            gammaOperation.value = GammaScrollBar.Value;
            GammaValue.Text = ((double)GammaScrollBar.Value / 100.0).ToString();
            PreviewPictureBox.Image = gammaOperation.Execute();
            contrastOperation.UpdateImage(PreviewPictureBox.Image, ContrastScrollBar.Value);
            brightnessOperation.UpdateImage(PreviewPictureBox.Image, BrightnessScrollBar.Value);
            isGamma = true;
            isBrightness = false;
            isContrast = false;
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            if (isBrightness)
                operations.Add(new Dictionary<string, int>() { { "brightness", BrightnessScrollBar.Value } });
            if (isContrast)
                operations.Add(new Dictionary<string, int>() { { "contrast", ContrastScrollBar.Value } });
            if (isGamma)
                operations.Add(new Dictionary<string, int>() { { "gamma", GammaScrollBar.Value } });

            modifiedImage = (Image)loadedImage.Clone();

            brightnessOperation = new BrightnessOperation(modifiedImage);
            contrastOperation = new ContrastOperation(modifiedImage);
            gammaOperation = new GammaOperation(modifiedImage);

            foreach (Dictionary<string, int> dict in operations)
            {
                if (dict.Keys.First() == "brightness")
                {
                    brightnessOperation.value = dict.Values.First();
                    modifiedImage = brightnessOperation.Execute();
                    brightnessOperation.previousValue = brightnessOperation.value;

                    if (contrastOperation.previousValue != 100)
                        contrastOperation.UpdateImage(modifiedImage, (int)contrastOperation.value);
                    else
                        contrastOperation.UpdateImage(modifiedImage);
                    if (gammaOperation.previousValue != 100)
                        gammaOperation.UpdateImage(modifiedImage, (int)gammaOperation.value);
                    else
                        gammaOperation.UpdateImage(modifiedImage);
                }
                if (dict.Keys.First() == "contrast")
                {
                    contrastOperation.value = dict.Values.First();
                    modifiedImage = contrastOperation.Execute();
                    contrastOperation.previousValue = contrastOperation.value;

                    if (brightnessOperation.previousValue != 0)
                        brightnessOperation.UpdateImage(modifiedImage, (int)brightnessOperation.value);
                    else
                        brightnessOperation.UpdateImage(modifiedImage);
                    if (gammaOperation.previousValue != 100)
                        gammaOperation.UpdateImage(modifiedImage, (int)gammaOperation.value);
                    else
                        gammaOperation.UpdateImage(modifiedImage);
                }
                if (dict.Keys.First() == "gamma")
                {
                    gammaOperation.value = dict.Values.First();
                    modifiedImage = gammaOperation.Execute();
                    gammaOperation.previousValue = gammaOperation.value;

                    if (brightnessOperation.previousValue != 0)
                        brightnessOperation.UpdateImage(modifiedImage, (int)brightnessOperation.value);
                    else
                        brightnessOperation.UpdateImage(modifiedImage);
                    if (contrastOperation.previousValue != 100)
                        contrastOperation.UpdateImage(modifiedImage, (int)contrastOperation.value);
                    else
                        contrastOperation.UpdateImage(modifiedImage);
                }
            }
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void resetButton_Click(object sender, EventArgs e)
        {
            BrightnessScrollBar.Value = 0;
            BrightnessValue.Text = "0";
            ContrastScrollBar.Value = 100;
            ContrastValue.Text = "1";
            GammaScrollBar.Value = 100;
            GammaValue.Text = "1";
            // reset operation objects
            PreviewPictureBox.Image = previewImage;
            brightnessOperation = new BrightnessOperation(PreviewPictureBox.Image);
            contrastOperation = new ContrastOperation(PreviewPictureBox.Image);
            gammaOperation = new GammaOperation(PreviewPictureBox.Image);
        }
    }
}
