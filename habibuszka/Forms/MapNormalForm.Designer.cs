﻿namespace habibuszka
{
    partial class MapNormalForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.StrengthTextBox = new System.Windows.Forms.TextBox();
            this.StrengthLabel = new System.Windows.Forms.Label();
            this.OkButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // StrengthTextBox
            // 
            this.StrengthTextBox.Location = new System.Drawing.Point(81, 21);
            this.StrengthTextBox.Name = "StrengthTextBox";
            this.StrengthTextBox.Size = new System.Drawing.Size(100, 20);
            this.StrengthTextBox.TabIndex = 5;
            // 
            // StrengthLabel
            // 
            this.StrengthLabel.AutoSize = true;
            this.StrengthLabel.Location = new System.Drawing.Point(21, 24);
            this.StrengthLabel.Name = "StrengthLabel";
            this.StrengthLabel.Size = new System.Drawing.Size(47, 13);
            this.StrengthLabel.TabIndex = 4;
            this.StrengthLabel.Text = "Strength";
            // 
            // OkButton
            // 
            this.OkButton.Location = new System.Drawing.Point(152, 79);
            this.OkButton.Name = "OkButton";
            this.OkButton.Size = new System.Drawing.Size(75, 23);
            this.OkButton.TabIndex = 3;
            this.OkButton.Text = "OK";
            this.OkButton.UseVisualStyleBackColor = true;
            this.OkButton.Click += new System.EventHandler(this.OkButton_Click);
            // 
            // MapNormalForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(239, 114);
            this.Controls.Add(this.StrengthTextBox);
            this.Controls.Add(this.StrengthLabel);
            this.Controls.Add(this.OkButton);
            this.Name = "MapNormalForm";
            this.Text = "MapNormalForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox StrengthTextBox;
        private System.Windows.Forms.Label StrengthLabel;
        private System.Windows.Forms.Button OkButton;
    }
}