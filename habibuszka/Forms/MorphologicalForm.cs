﻿using habibuszka.Transformations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace habibuszka
{
    public partial class MorphologicalForm : Form
    {
        private Image image;
        public Image modifiedImage;
        private MorpholigicalType type;
        public MorphologicalForm(Image image, MorpholigicalType type)
        {
            this.image = image;
            this.type = type;
            InitializeComponent();
        }

        private void OkButton_Click(object sender, EventArgs e)
        {
            int maskSize = ((SizeComboBox.SelectedIndex + 1) * 2) + 1;
            MorphologicalOperator morOperator = new MorphologicalOperator(image);
            SE shape;
            Enum.TryParse<SE>(ShapeComboBox.SelectedValue.ToString(), out shape); 
            modifiedImage = morOperator.Transform(maskSize, shape, this.type);
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
    }
}
