﻿using habibuszka.Transformations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace habibuszka
{
    public partial class EdgeCannyForm : Form
    {
        private Image image;
        public Image modifiedImage;
        public EdgeCannyForm(Image image)
        {
            this.image = image;
            InitializeComponent();
        }

        private void OkButton_Click(object sender, EventArgs e)
        {
            int lowerThreshold = int.Parse(LowerThresholdTextBox.Text);
            int upperThreshold = int.Parse(UpperThresholdTextBox.Text);
            EdgeCanny edge = new EdgeCanny(new Bitmap(image));
            modifiedImage = edge.Transform(lowerThreshold, upperThreshold);
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
    }
}
