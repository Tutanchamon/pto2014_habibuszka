﻿using habibuszka.Exceptions;
using habibuszka.Transformations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace habibuszka
{
    public partial class ManualBinForm : Form
    {
        private Image image;
        public Image modifiedImage;
        public ManualBinForm(Image image)
        {
            InitializeComponent();
            this.image = image;
        }

        private void OkButton_Click(object sender, EventArgs e)
        {
            try
            {
                int val = int.Parse(ThresholdTextBox.Text);
                ManualBin mb = new ManualBin(new Bitmap(image));
                Bitmap bmp = mb.Execute(val);
                modifiedImage = (Image)bmp;
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            catch
            {
                new HabibuszkaMainException("Podaj wartość z przedziału [0, 255].");
            }
        }
    }
}
