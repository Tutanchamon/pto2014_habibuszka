﻿using habibuszka.Exceptions;
using habibuszka.Transformations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace habibuszka
{
    public partial class NoiseBilateralForm : Form
    {
        private Image image;
        public Image modifiedImage;
        public NoiseBilateralForm(Image image)
        {
            InitializeComponent();
            this.image = image;
        }

        private void OkButton_Click(object sender, EventArgs e)
        {
            try
            {
                int sigmaD = int.Parse(SigmaDTextBox.Text);
                int sigmaR = int.Parse(SigmaRTextBox.Text);
                NoiseBilateral mb = new NoiseBilateral(new Bitmap(image), sigmaD, sigmaR);
                modifiedImage = (Image)mb.Transform();
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            catch
            {
                new HabibuszkaMainException("Podaj prawidłowe wartości.");
            }
        }
    }
}
