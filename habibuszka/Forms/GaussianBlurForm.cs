﻿using habibuszka.Exceptions;
using habibuszka.Transformations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace habibuszka
{
    public partial class GaussianBlurForm : Form
    {
        private Image image;
        public Image modifiedImage;
        public GaussianBlurForm(Image image)
        {
            InitializeComponent();
            this.image = image;
            MaskComboBox.SelectedIndex = 0;
        }

        private void OkButton_Click(object sender, EventArgs e)
        {
            int maskSize = MaskComboBox.SelectedIndex + 1;
            try
            {
                double sigma = double.Parse(SigmaTextBox.Text);
                Convolution c = new Convolution(new Bitmap(image));
                Bitmap img = c.Convolute(GaussianBlur.GetMask(maskSize, sigma), Mode.RepeatEdge);
                modifiedImage = img;
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            catch
            {
                new HabibuszkaMainException("Niewłaściwa wartość sigma");
            }
            
        }
    }
}
