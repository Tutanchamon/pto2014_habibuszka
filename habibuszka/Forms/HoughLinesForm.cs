﻿using habibuszka.Exceptions;
using habibuszka.Transformations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace habibuszka
{
    public partial class HoughLinesForm : Form
    {
        private Image image;
        public Image modifiedImage;
        public HoughLinesForm(Image image)
        {
            this.image = image;
            InitializeComponent();
        }

        private void OkButton_Click(object sender, EventArgs e)
        {
            try
            {
                int threshold = int.Parse(ThresholdTextBox.Text);
                bool lines = LinesCheckBox.Checked;
                HoughLines hough = new HoughLines(new Bitmap(image));
                modifiedImage = hough.Transform(threshold, lines);
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            catch
            {
                new HabibuszkaMainException("Podaj prawidłową wartość.");
            }
        }
    }
}
