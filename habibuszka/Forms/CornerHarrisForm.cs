﻿using habibuszka.Exceptions;
using habibuszka.Transformations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace habibuszka
{
    public partial class CornerHarrisForm : Form
    {
        public Image image;
        public Image modifiedImage;
        public CornerHarrisForm(Image image)
        {
            InitializeComponent();
            this.image = image;
        }

        private void OkButton_Click(object sender, EventArgs e)
        {
            try
            {
                int threshold = int.Parse(ThresholdTextBox.Text);
                double sigma = double.Parse(SigmaTextBox.Text);
                double weight = double.Parse(WeightTextBox.Text);
                double k = double.Parse(KTextBox.Text);
                CornerHarris cornerHarris = new CornerHarris(new Bitmap(image));
                modifiedImage = cornerHarris.Transform(threshold, sigma, weight, k);
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            catch
            {
                new HabibuszkaMainException("Niewłaściwa wartość sigma");
            }
        }
    }
}
