﻿using habibuszka.Hist;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace habibuszka
{
    public partial class HistogramForm : Form
    {
        public Image image;
        private Histogram histogram;
        public HistogramForm(Image image, bool grayScale)
        {
            InitializeComponent();
            histogram = new Histogram(new Bitmap(image));
            histogram.Generate();
            Console.WriteLine(histogram.rDict);
            RedrawHistogram();

            if (grayScale)
            {
                redCheckBox.Visible = false;
                redCheckBox.Checked = false;
                blueCheckBox.Visible = false;
                blueCheckBox.Checked = false;
                greenCheckBox.Visible = false;
                greenCheckBox.Checked = false;
            }
            else
            {
                luminosityChckBox.Checked = false;
                luminosityChckBox.Visible = false;
            }
        }

        private void RedrawHistogram()
        {
            histogramBox.Image = new Bitmap(256, 138);
            if(redCheckBox.Checked)
                histogramBox.Image = (Image)histogram.GenerateHistogram(histogram.rDict, Pens.Red, new Bitmap(histogramBox.Image));
            if (greenCheckBox.Checked)
                histogramBox.Image = (Image)histogram.GenerateHistogram(histogram.gDict, Pens.Green, new Bitmap(histogramBox.Image));
            if (blueCheckBox.Checked)
                histogramBox.Image = (Image)histogram.GenerateHistogram(histogram.bDict, Pens.Blue, new Bitmap(histogramBox.Image));
            if (luminosityChckBox.Checked)
            {
                histogram.lDict = histogram.bDict;
                histogramBox.Image = (Image)histogram.GenerateHistogram(histogram.lDict, Pens.Black, new Bitmap(histogramBox.Image));
            }
                
        }
        private void redCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            RedrawHistogram();
        }

        private void greenCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            RedrawHistogram();
        }

        private void blueCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            RedrawHistogram();
        }

        public void Test()
        {
            Bitmap bmp = new Bitmap(@"c:\Blazej\Rozne\Obrazki\sylwester2012\fireworks-1.jpg");
            int[] histogram_r = new int[256];
            int[] histogram_g = new int[256];
            int[] histogram_b = new int[256];
            float max = 0;

            for (int i = 0; i < bmp.Width; i++)
            {
                for (int j = 0; j < bmp.Height; j++)
                {
                    int redValue = bmp.GetPixel(i, j).R;
                    int blueValue = bmp.GetPixel(i, j).B;
                    int greenValue = bmp.GetPixel(i, j).G;
                    histogram_r[redValue]++;
                    histogram_b[blueValue]++;
                    histogram_g[greenValue]++;
                    if (max < histogram_r[redValue])
                        max = histogram_r[redValue];
                    if (max < histogram_b[blueValue])
                        max = histogram_b[blueValue];
                    if (max < histogram_g[greenValue])
                        max = histogram_g[greenValue];
                }
            }

            int histHeight = 512;
            Bitmap img = new Bitmap(256, histHeight + 10);
            using (Graphics g = Graphics.FromImage(img))
            {
                for (int i = 0; i < histogram_r.Length; i++)
                {
                    Pen pen = chooseMaximumPen(i, histogram_r, histogram_g, histogram_b);
                    float pct = chooseMaximumValue(i, histogram_r, histogram_g, histogram_b) / max;   // What percentage of the max is this value?
                    g.DrawLine(pen,
                        new Point(i, img.Height - 5),
                        new Point(i, img.Height - 5 - (int)(pct * histHeight))  // Use that percentage of the height
                        );
                }
            }
            img.Save(@"c:\temp\test.jpg");
        }

        private Pen chooseMaximumPen(int index, int[] red, int[] green, int[] blue)
        {
            if (red[index] >= green[index] && red[index] >= blue[index]) {
                return Pens.Red;
            }
            if (blue[index] >= green[index] && blue[index] >= red[index]) {
                return Pens.Blue;
            }
            if (green[index] >= red[index] && blue[index] >= red[index])
            {
                return Pens.Green;
            }
            else return Pens.Blue;
        }

        private int chooseMaximumValue(int index, int[] red, int[] green, int[] blue)
        {
            if (red[index] >= green[index] && red[index] >= blue[index])
            {
                return red[index];
            }
            if (blue[index] >= green[index] && blue[index] >= red[index])
            {
                return blue[index];
            }
            if (green[index] >= red[index] && blue[index] >= red[index])
            {
                return green[index];
            }
            return blue[index];
        }

        private void luminosityChckBox_CheckedChanged(object sender, EventArgs e)
        {
            RedrawHistogram();
        }
     }
 }

