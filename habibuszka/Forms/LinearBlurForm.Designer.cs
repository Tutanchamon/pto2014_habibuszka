﻿using System;
using System.Data;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using System.Collections.Generic;
namespace habibuszka
{
    partial class LinearBlurForm
    {
        private System.Windows.Forms.Label sizeLabel;
        private System.Windows.Forms.Button btLoad;
        private habibuszka.Extensions.GridArray gridArray1;


		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LinearBlurForm));
            this.sizeLabel = new System.Windows.Forms.Label();
            this.btLoad = new System.Windows.Forms.Button();
            this.gridArray1 = new habibuszka.Extensions.GridArray();
            this.OkButton = new System.Windows.Forms.Button();
            this.normalizeCheckBox = new System.Windows.Forms.CheckBox();
            this.MaskSizeComboBox = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // sizeLabel
            // 
            this.sizeLabel.Location = new System.Drawing.Point(8, 28);
            this.sizeLabel.Name = "sizeLabel";
            this.sizeLabel.Size = new System.Drawing.Size(44, 23);
            this.sizeLabel.TabIndex = 14;
            this.sizeLabel.Text = "Size:";
            this.sizeLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btLoad
            // 
            this.btLoad.Location = new System.Drawing.Point(155, 28);
            this.btLoad.Name = "btLoad";
            this.btLoad.Size = new System.Drawing.Size(75, 23);
            this.btLoad.TabIndex = 12;
            this.btLoad.Text = "Load";
            this.btLoad.Click += new System.EventHandler(this.btLoad_Click);
            // 
            // gridArray1
            // 
            this.gridArray1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridArray1.AutoSizeMinHeight = 10;
            this.gridArray1.AutoSizeMinWidth = 10;
            this.gridArray1.AutoStretchColumnsToFitWidth = false;
            this.gridArray1.AutoStretchRowsToFitHeight = false;
            this.gridArray1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.gridArray1.ContextMenuStyle = ((SourceGrid2.ContextMenuStyle)((SourceGrid2.ContextMenuStyle.ClearSelection | SourceGrid2.ContextMenuStyle.CopyPasteSelection)));
            this.gridArray1.EnableEdit = true;
            this.gridArray1.FocusStyle = SourceGrid2.FocusStyle.None;
            this.gridArray1.GridToolTipActive = true;
            this.gridArray1.Location = new System.Drawing.Point(8, 56);
            this.gridArray1.Name = "gridArray1";
            this.gridArray1.Size = new System.Drawing.Size(418, 200);
            this.gridArray1.SpecialKeys = ((SourceGrid2.GridSpecialKeys)(((((((((SourceGrid2.GridSpecialKeys.Ctrl_C | SourceGrid2.GridSpecialKeys.Ctrl_V) 
            | SourceGrid2.GridSpecialKeys.Ctrl_X) 
            | SourceGrid2.GridSpecialKeys.Delete) 
            | SourceGrid2.GridSpecialKeys.Arrows) 
            | SourceGrid2.GridSpecialKeys.Tab) 
            | SourceGrid2.GridSpecialKeys.PageDownUp) 
            | SourceGrid2.GridSpecialKeys.Enter) 
            | SourceGrid2.GridSpecialKeys.Escape)));
            this.gridArray1.TabIndex = 15;
            // 
            // OkButton
            // 
            this.OkButton.Location = new System.Drawing.Point(351, 28);
            this.OkButton.Name = "OkButton";
            this.OkButton.Size = new System.Drawing.Size(75, 23);
            this.OkButton.TabIndex = 16;
            this.OkButton.Text = "OK";
            this.OkButton.UseVisualStyleBackColor = true;
            this.OkButton.Click += new System.EventHandler(this.OkButton_Click);
            // 
            // normalizeCheckBox
            // 
            this.normalizeCheckBox.AutoSize = true;
            this.normalizeCheckBox.Location = new System.Drawing.Point(248, 33);
            this.normalizeCheckBox.Name = "normalizeCheckBox";
            this.normalizeCheckBox.Size = new System.Drawing.Size(72, 17);
            this.normalizeCheckBox.TabIndex = 17;
            this.normalizeCheckBox.Text = "Normalize";
            this.normalizeCheckBox.UseVisualStyleBackColor = true;
            // 
            // MaskSizeComboBox
            // 
            this.MaskSizeComboBox.DataSource = ((object)(resources.GetObject("MaskSizeComboBox.DataSource")));
            this.MaskSizeComboBox.DisplayMember = "Key";
            this.MaskSizeComboBox.FormattingEnabled = true;
            this.MaskSizeComboBox.Location = new System.Drawing.Point(58, 30);
            this.MaskSizeComboBox.Name = "MaskSizeComboBox";
            this.MaskSizeComboBox.Size = new System.Drawing.Size(72, 21);
            this.MaskSizeComboBox.TabIndex = 18;
            this.MaskSizeComboBox.ValueMember = "Value";
            // 
            // LinearBlurForm
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(434, 260);
            this.Controls.Add(this.MaskSizeComboBox);
            this.Controls.Add(this.normalizeCheckBox);
            this.Controls.Add(this.OkButton);
            this.Controls.Add(this.gridArray1);
            this.Controls.Add(this.sizeLabel);
            this.Controls.Add(this.btLoad);
            this.Name = "LinearBlurForm";
            this.Text = "Linear Blur";
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion

        private Button OkButton;
        private CheckBox normalizeCheckBox;
        private ComboBox MaskSizeComboBox;
        private IContainer components;

		
	}
}