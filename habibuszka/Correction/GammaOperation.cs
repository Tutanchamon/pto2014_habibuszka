﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace habibuszka.Correction
{
    public class GammaOperation : CorrectionOperation
    {
        public GammaOperation(Image image)
            : base(image)
        {
            previousValue = 100;
        }

        protected override void CalculateValuesTable()
        {
            for (int i = 0; i < 256; i++)
            {
                values[i] = (int)Math.Pow((double)i, ((value - previousValue + 100.0) / 100.0));
                if (values[i] > 255)
                    values[i] = 255;
                else if (values[i] < 0)
                    values[i] = 0;
            }
        }
    }
}
