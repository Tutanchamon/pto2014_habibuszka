﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace habibuszka.Correction
{
    public abstract class CorrectionOperation : AbstractOperation
    {
        public double value { set; get; }

        protected int[] values;

        public CorrectionOperation(Image image)
            : base(image)
        {
            values = new int[256];
            for (int i = 0; i < 256; i++)
            {
                values[i] = i;
            }
        }

        protected abstract void CalculateValuesTable();

        public override Bitmap Execute()
        {
            Bitmap bmp = (Bitmap)bitmap.Clone();
            CalculateValuesTable();
            int width = bitmap.Width;
            int height = bitmap.Height;
            try
            {
                for (int i = 0; i < width; i++)
                {
                    for (int j = 0; j < height; j++)
                    {
                        Color original = bmp.GetPixel(i, j);
                        int red = values[original.R];
                        int green = values[original.G];
                        int blue = values[original.B];
                        Color newColor = Color.FromArgb(red, green, blue);
                        bmp.SetPixel(i, j, newColor);

                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return bmp;
        }
    }
}
