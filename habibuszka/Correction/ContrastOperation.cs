﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace habibuszka.Correction
{
    public class ContrastOperation : CorrectionOperation
    {
        public ContrastOperation(Image image)
            : base(image)
        {
            previousValue = 100;
        }

        protected override void CalculateValuesTable()
        {
            for (int i = 0; i < 256; i++)
            {
                values[i] = (int)(i * ((value - previousValue + 100.0) / 100.0));
                if (values[i] > 255)
                    values[i] = 255;
                else if (values[i] < 0)
                    values[i] = 0;
            }
        }
    }
}
