﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace habibuszka.Exceptions
{
    public class HabibuszkaMainException : Exception
    {
        public HabibuszkaMainException()
        {
            MessageBox.Show("Wystąpił błąd", "Błąd!", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
        }

        public HabibuszkaMainException(string message)
        {
            MessageBox.Show(message, "Błąd!", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
        }
    }
}
