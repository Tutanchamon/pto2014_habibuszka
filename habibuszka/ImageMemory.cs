﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToolStripMenuItem = System.Windows.Forms.ToolStripMenuItem;

namespace habibuszka
{
    class ImageMemory
    {
        private ToolStripMenuItem menuItem;
        private Stack<Image> undoStack;
        private Stack<Image> redoStack;

        public ImageMemory(ToolStripMenuItem menuItem)
        {
            this.menuItem = menuItem;
            undoStack = new Stack<Image>();
            redoStack = new Stack<Image>();
        }

        public int GetUndoCount() {
            return undoStack.Count;
        }

        public Image Undo(Image image)
        {
            if (undoStack.Count > 0) 
            {
                Image toReturn = undoStack.Pop();
                redoStack.Push(image);
                return toReturn;
            }
            return null;
        }

        public Image Redo(Image image)
        {
            if (redoStack.Count > 0)
            {
                Image img = redoStack.Pop();
                undoStack.Push(image);
                return img;
            }
            return null;
        }

        public void Execute(Image img)
        {
            menuItem.Enabled = true;
            undoStack.Push(img);
        }

        public int GetRedoCount()
        {
            return redoStack.Count;
        }
    }
}
