﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace habibuszka
{
    class NegativeOperation : AbstractOperation
    {
        public NegativeOperation(Image image) : base(image) { }
        public override Bitmap Execute()
        {
            int width = bitmap.Width;
            int height = bitmap.Height;
            try
            {
                for (int i = 0; i < width; i++)
                {
                    for (int j = 0; j < height; j++)
                    {
                        Color original = bitmap.GetPixel(i,j);
                        int red = 255 - original.R;
                        int green = 255 - original.G;
                        int blue = 255 - original.B;
                        Color newColor = Color.FromArgb(red, green, blue);
                        bitmap.SetPixel(i, j, newColor);

                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return bitmap;
        }
    }
}
