﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace habibuszka
{
    public class GrayscaleOperation : AbstractOperation
    {
        public GrayscaleOperation(Image image)
            : base(image) { }
        public override Bitmap Execute()
        {
            int width = bitmap.Width;
            int height = bitmap.Height;
            try
            {
                for (int i = 0; i < width; i++)
                {
                    for (int j = 0; j < height; j++)
                    {
                        Color original = bitmap.GetPixel(i, j);
                        int avg = (int)(original.R * 0.3 + original.G * 0.6 + original.B * 0.1);
                        Color newColor = Color.FromArgb(avg, avg, avg);
                        bitmap.SetPixel(i, j, newColor);

                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return bitmap;
        }
    }
}
