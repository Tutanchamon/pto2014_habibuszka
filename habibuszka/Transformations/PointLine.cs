﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace habibuszka.Transformations
{
    public class PointLine
    {

        public int x1 { get; set; }
        public int x2 { get; set; }
        public int y1 { get; set; }
        public int y2 { get; set; }

        public PointLine(int x1, int y1, int x2, int y2)
        {
            this.x1 = x1;
            this.y1 = y1;
            this.x2 = x2;
            this.y2 = y2;
        }
        override
        public String ToString()
        {
            return "(" + x1 + ", " + y1 + ") (" + x2 + ", " + y2 + ")";
        }

        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to PointLine return false.
            PointLine p = obj as PointLine;
            if ((System.Object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (x1 == p.x1) && (y1 == p.y1) && (x2 == p.x2) && (y2 == p.y2);
        }

        public bool Equals(PointLine p)
        {
            // If parameter is null return false:
            if ((object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (x1 == p.x1) && (y1 == p.y1) && (x2 == p.x2) && (y2 == p.y2);
        }

        public override int GetHashCode()
        {
            return x1 ^ y1 ^ x2 ^ y2;
        }
    }
}
