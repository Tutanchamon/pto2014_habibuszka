﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace habibuszka.Transformations
{
    class MorphologicalOperatorTest
    {
        public void TestMatrices()
        {
            /*
            MorphologicalOperator mor = new MorphologicalOperator(null);
            bool[,] matrix = mor.GetSE(3, MorphologicalOperator.SE.Square);
            writeMatrix(matrix, "3x3 square");
            matrix = mor.GetSE(5, MorphologicalOperator.SE.XCross);
            writeMatrix(matrix, "5x5 XCross");
            matrix = mor.GetSE(5, MorphologicalOperator.SE.Cross);
            writeMatrix(matrix, "5x5 cross");
            matrix = mor.GetSE(7, MorphologicalOperator.SE.HLine);
            writeMatrix(matrix, "7x7 HLine");
            matrix = mor.GetSE(3, MorphologicalOperator.SE.VLine);
            writeMatrix(matrix, "3x3 VLine");
             * */
        }

        private void writeMatrix(bool[,] matrix, String title)
        {
            Console.WriteLine("======");
            Console.WriteLine(title);
            Console.WriteLine("======");
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                Console.Write("|");
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    if (matrix[i, j])
                    {
                        Console.Write("x");
                    }
                    else {
                        Console.Write(" ");
                    }
                    Console.Write("|");
                    
                }
                Console.WriteLine();
            }
        }
    }
}
