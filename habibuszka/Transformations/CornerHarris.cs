﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace habibuszka.Transformations
{
    public class CornerHarris : Convolution
    {
        public List<Point> foundPoints { get; set; }
        public CornerHarris(Bitmap bitmap) : base(bitmap) {
            foundPoints = new List<Point>();
        }

        public Bitmap Transform(int threshold, double sigma, double sigmaWeight, double kParam)
        {
            Bitmap newBitmap = (Bitmap)bitmap.Clone();

            int width = bitmap.Width;
            int height = bitmap.Height;

            newBitmap = new GrayscaleOperation(newBitmap).Execute();
            newBitmap = Convolute(GaussianBlur.GetMask(3, 1.6), Mode.RepeatEdge);
            double[,] ixx = new double[width, height];
            double[,] ixy = new double[width, height];
            double[,] iyy = new double[width, height];
            double[,] cornerCandidates = new double[width, height];
            double[,] cornerNonmaxSuppress = new double[width, height]; ;

            EdgeSobel edgeSobel = new EdgeSobel(newBitmap);
            double[,] gx = edgeSobel.RawHorizontalDetection();
            double[,] gy = edgeSobel.RawVerticalDetection();

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    ixx[i, j] = gx[i, j] * gx[i, j];
                    iyy[i, j] = gy[i, j] * gy[i, j];
                    ixy[i, j] = gx[i, j] * gy[i, j];

                }
            }

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    if (i != 0 && i != (width - 1) && j != 0 && j != (height - 1))
                    {
                        double sxx = 0;
                        double syy = 0;
                        double sxy = 0;
                        for (int k = -1; k <= 1; k++)
                        {
                            for (int l = -1; l <= 1; l++)
                            {
                                double gaussValue = GaussianBlur.GetGauss(k, l, sigma);

                                sxx = sxx + (ixx[i + k, j + l] * gaussValue);
                                syy = syy + (iyy[i + k, j + l] * gaussValue);
                                sxy = sxy + (ixy[i + k, j + l] * gaussValue);
                            }
                        }
                        sxx /= sigmaWeight;
                        syy /= sigmaWeight;
                        sxy /= sigmaWeight;
                        double[,] hMatrix = new double[2, 2];
                        hMatrix[0, 0] = sxx;
                        hMatrix[1, 1] = syy;
                        hMatrix[0, 1] = sxy;
                        hMatrix[1, 0] = sxy;
                        double r = hMatrix[0, 0] * hMatrix[1, 1] - hMatrix[0, 1] * hMatrix[1, 0] - kParam * Math.Pow(hMatrix[0, 0] + hMatrix[1, 1], 2);
                        if (r > threshold)
                            cornerCandidates[i, j] = r;
                        else
                            cornerCandidates[i, j] = 0;
                    }

                }
            }

            bool search = true;
            //8
            while (search == true)
            {
                search = false;
                for (int i = 1; i < (width - 1); i++)
                {
                    for (int j = 1; j < (height - 1); j++)
                    {
                        double val = cornerCandidates[i, j];
                        if ((val > cornerCandidates[i - 1, j - 1]) && (val > cornerCandidates[i - 1, j]) && (val > cornerCandidates[i - 1, j + 1])
                            && (val > cornerCandidates[i, j - 1]) && (val > cornerCandidates[i, j + 1]) &&
                            (val > cornerCandidates[i + 1, j - 1]) && (val > cornerCandidates[i + 1, j]) && (val > cornerCandidates[i + 1, j + 1]))
                        {
                            cornerNonmaxSuppress[i, j] = val;
                        }
                        else
                        {
                            if (val > 0)
                                search = true;
                            cornerNonmaxSuppress[i, j] = 0;
                        }
                    }
                }
                cornerCandidates = cornerNonmaxSuppress;
            }
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    if (cornerCandidates[i, j] == 0)
                        newBitmap.SetPixel(i, j, Color.Black);
                    else {
                        newBitmap.SetPixel(i, j, Color.White);
                        foundPoints.Add(new Point(i, j));
                    }
                }
            }

            return newBitmap;
        }


    }
}
