﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace habibuszka.Transformations
{
    public class HoughLines : Convolution
    {
        // list of detected lines - needed for rectangle recognition
        public List<PointLine> lines { get; set; }
        public HoughLines(Bitmap bitmap) : base(bitmap) {
            lines = new List<PointLine>();
        }

        public Bitmap Transform(int threshold, bool drawWholeLines)
        {
            Bitmap newBitmap = (Bitmap)bitmap.Clone();
            EdgeLaplacian edge = new EdgeLaplacian(bitmap);
            Bitmap edgeBitmap = edge.Convolute(edge.GetMask(3));
            GradientBin bin = new GradientBin(edgeBitmap);
            Bitmap binBitmap = bin.Execute(150);
            Hough hough = new Hough(binBitmap);
            Bitmap houghBitmap = hough.Transform(3, true);
            Graphics graphics = Graphics.FromImage(newBitmap);
            Pen pen = new Pen(Color.Red, 1);

            // iterate over pixels of the SPECTRE
            Console.WriteLine("HoughBitmap size: " + houghBitmap.Width + " x " + houghBitmap.Height);
            for (int i = 1; i < houghBitmap.Width; i++)
            {
                for (int j = 0; j < houghBitmap.Height; j++)
                {
                    Color actualPixelColor = houghBitmap.GetPixel(i, j);
                    if (actualPixelColor.R > threshold)
                    {
                        double rtheta = ((double)i / 3.0) * Math.PI / 180.0;
                        int real_rho = j - houghBitmap.Height / 2;
                        double sint = Math.Sin(rtheta);
                        graphics.DrawLine(pen, 0, (int)(real_rho / sint), newBitmap.Width - 1, (int)((real_rho - (newBitmap.Width - 1) * Math.Cos(rtheta)) / sint));
                        // add line to the list of detected lines
                        /*PointLine pointLine = new PointLine(0, (int)(real_rho / sint), newBitmap.Width - 1, (int)((real_rho - (newBitmap.Width - 1) * Math.Cos(rtheta)) / sint));
                        if (BelongsToImage(pointLine, newBitmap.Width, newBitmap.Height))
                        {
                            if (!lines.Contains(pointLine))
                            {
                                Console.WriteLine(pointLine);
                                lines.Add(pointLine);
                            }
                            
                        }
                         * */
                        //Console.WriteLine(pointLine.ToString());
                        
                    }
                }
            }

            if (drawWholeLines == false)
            {
                for (int x = 0; x < binBitmap.Width; x++)
                {
                    for (int y = 0; y < binBitmap.Height; y++)
                    {
                        if (binBitmap.GetPixel(x, y).R == 0)
                        {
                            newBitmap.SetPixel(x, y, bitmap.GetPixel(x, y));
                        }
                    }
                }
            }

            return newBitmap;
        }

        private bool BelongsToImage(PointLine pointLine, int width, int height)
        {
            if (pointLine.x1 < 0 || pointLine.x1 > width)
                return false;
            if (pointLine.x2 < 0 || pointLine.x2 > width)
                return false;
            if (pointLine.y1 < 0 || pointLine.y1 > height)
                return false;
            if (pointLine.y2 < 0 || pointLine.y2 > height)
                return false;
            return true;
        }
    }
}