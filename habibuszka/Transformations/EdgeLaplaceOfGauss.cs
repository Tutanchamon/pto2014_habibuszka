﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace habibuszka.Transformations
{
    public class EdgeLaplaceOfGauss : Convolution
    {
        public EdgeLaplaceOfGauss(Bitmap bitmap)
            : base(bitmap)
        { }

        private double GetLoG(int x, int y, double s)
        {
            double multiplier = (Math.Pow(x, 2) + Math.Pow(y, 2) - 2.0) / Math.Pow(s, 2);
            return multiplier * GaussianBlur.GetGauss(x, y, s);
        }

        public double[,] GetMask(int size, double sigma, Mode mode = Mode.RepeatEdge)
        {
            double[,] matrix = new double[size, size];
            int radius = (size - 1) / 2;
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    int x = i - radius;
                    int y = j - radius;
                    matrix[i, j] = GetLoG(x, y, sigma);
                }
            }
            return matrix;
        }
    }
}
