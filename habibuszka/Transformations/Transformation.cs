﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace habibuszka.Transformations
{
    public class Transformation
    {
        protected Bitmap image;

        public Transformation(Image image)
        {
            this.image = new Bitmap(image);
        }

        protected Color GetPixel(int x, int y, Mode mode = Mode.RepeatEdge)
        {
            switch (mode)
            {
                case Mode.CyclicEdge:
                    return GetPixelCyclic(x, y);

                case Mode.NullEdge:
                    return GetPixelNull(x, y);

                case Mode.RepeatEdge:
                    return GetPixelRepeat(x, y);

                default:
                    return new Color();
            }
        }

        private Color GetPixelCyclic(int x, int y)
        {
            return image.GetPixel((x % image.Width + image.Width) % image.Width, (y % image.Height + image.Height) % image.Height);
        }

        private Color GetPixelNull(int x, int y)
        {
            if (x < 0 || y < 0 || x > image.Width - 1 || y > image.Height - 1)
                return Color.FromArgb(0, 0, 0); 
            else
                return image.GetPixel(x, y);
        }

        private Color GetPixelRepeat(int x, int y)
        {
            if(x < 0)
            {
                if (y < 0)
                    return image.GetPixel(0, 0);
                else if (y > image.Height - 1)
                    return image.GetPixel(0, image.Height - 1);
                else
                    return image.GetPixel(0, y);
            }
            else if(x > image.Width - 1)
            {
                if (y < 0)
                    return image.GetPixel(image.Width - 1, 0);
                else if (y > image.Height - 1)
                    return image.GetPixel(image.Width - 1, image.Height - 1);
                else
                    return image.GetPixel(image.Width - 1, y);
            }
            else
            {
                if (y < 0)
                    return image.GetPixel(x, 0);
                else if (y > image.Height - 1)
                    return image.GetPixel(x, image.Height - 1);
                else
                    return image.GetPixel(x, y);
            }
        }

        public double[,] GetWindow(int x, int y, int size, Channel channel, Mode mode = Mode.RepeatEdge)
        {
            double[,] toReturn = new double[size, size];
            int indexX = x - (size / 2);
            int indexY = y - (size / 2);
            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    switch (channel)
                    {
                        case Channel.Red:
                            toReturn[i, j] = GetPixel(indexX + i, indexY + j, mode).R;
                            break;
                        case Channel.Green:
                            toReturn[i, j] = GetPixel(indexX + i, indexY + j, mode).G;
                            break;
                        case Channel.Blue:
                            toReturn[i, j] = GetPixel(indexX + i, indexY + j, mode).B;
                            break;
                    }
                }
            }
            return toReturn;
        }
    }

    public enum Channel
    {
        Red,
        Green,
        Blue
    }

    public enum Mode
    {
        CyclicEdge,
        NullEdge,
        RepeatEdge
    }
}
