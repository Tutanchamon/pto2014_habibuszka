﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace habibuszka.Transformations
{
    public class EdgePrewitt : EdgeGradient
    {
        public EdgePrewitt(Bitmap bitmap)
            : base(bitmap)
        {
            PrepareMatrices();
        }

        private void PrepareMatrices()
        {
            g_x = new double[3, 3] {
                {-1, 0, 1},
		        {-1, 0, 1},
                {-1, 0, 1}
            };
            g_y = new double[3, 3] {
                {-1, -1, -1},
		        {0, 0, 0},
                {1, 1, 1}
            };
        }
    }
}
