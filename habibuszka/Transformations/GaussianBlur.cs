﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace habibuszka.Transformations
{
    class GaussianBlur
    {
        public static double[,] GetMask(int size, double sigma)
        {
            int radius = (size / 2) + 1;
            double[,] matrix = new double[2 * size + 1, 2 * size + 1];
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    matrix[i, j] = GetGauss(i - radius, j - radius, sigma);
                }
            }
            return matrix;
        }

        public static double GetGauss(int x, int y, double sigma)
        {
            double result = 0.0;
            double multiplier = (double)1.0 / (2 * Math.PI * sigma * sigma);
            double power = -(((x * x) + (y * y)) / (2 * sigma * sigma));
            result = multiplier * Math.Pow(Math.E, power);
            return result;
           
        }
    }
}
