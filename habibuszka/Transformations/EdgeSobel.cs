﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace habibuszka.Transformations
{
    public class EdgeSobel : EdgeGradient
    {
        public EdgeSobel(Bitmap bitmap)
            : base(bitmap)
        {
            PrepareMatrices();
        }

        private void PrepareMatrices()
        {
            g_x = new double[3, 3] {
                {-1, 0, 1},
		        {-2, 0, 2},
                {-1, 0, 1}
            };
            g_y = new double[3, 3] {
                {-1, -2, -1},
		        {0, 0, 0},
                {1, 2, 1}
            };
        }

        public double[,] RawHorizontalDetection()
        {
            GrayscaleOperation g = new GrayscaleOperation(bitmap);
            Transformation t = new Transformation(g.Execute());
            double[,] result = new double[bitmap.Width, bitmap.Height];
            for (int i = 0; i < bitmap.Width; i++)
            {
                for (int j = 0; j < bitmap.Height; j++)
                {
                    double[,] Wij = t.GetWindow(i, j, 3, Channel.Blue);
                    double[,] Aij = Join(g_x, Wij);
                    result[i, j] = Sum(Aij);
                }
            }
            return result;
        }
        public double[,] RawVerticalDetection()
        {
            GrayscaleOperation g = new GrayscaleOperation(bitmap);
            Transformation t = new Transformation(g.Execute());
            double[,] result = new double[bitmap.Width, bitmap.Height];
            for (int i = 0; i < bitmap.Width; i++)
            {
                for (int j = 0; j < bitmap.Height; j++)
                {
                    double[,] Wij = t.GetWindow(i, j, 3, Channel.Blue);
                    double[,] Aij = Join(g_y, Wij);
                    result[i, j] = Sum(Aij);
                }
            }
            return result;
        }

    }
}
