﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace habibuszka.Transformations
{
    public class NoiseBilateral : Transformation
    {
        private double sigmaR;
        private double sigmaD;

        public NoiseBilateral(Bitmap image, double sigmaD, double sigmaR)
            : base(image)
        {
            this.sigmaD = sigmaD;
            this.sigmaR = sigmaR;
        }

        private double SpatialCloseness(Point p1, Point p2)
        {
            double val1 = Math.Pow((p1.X - p2.X), 2) + Math.Pow((p1.Y - p2.Y), 2);
            double pow = -(val1 / (2 * Math.Pow(sigmaD, 2)));
            return Math.Pow(Math.E, pow);
        }

        private double ColorCloseness(int val1, int val2)
        {
            double pow = - (Math.Pow((val1 - val2), 2) / (2 * Math.Pow(sigmaR, 2)));
            return Math.Pow(Math.E, pow);
        }

        private int CalcVal(int x, int y, Channel channel)
        {
            int radius = (int)sigmaD;
            int size = (radius * 2) + 1;
            double[,] window = GetWindow(x, y, size, channel);
            int pixelVal = 0;
            switch (channel)
            {
                case Channel.Red:
                    pixelVal = GetPixel(x, y).R;
                    break;
                case Channel.Green:
                    pixelVal = GetPixel(x, y).G;
                    break;
                case Channel.Blue:
                    pixelVal = GetPixel(x, y).B;
                    break;
            }
            double numerator = 0;
            double denominator = 0;
            for (int i = 0; i < window.GetLength(0); i++)
            {
                for (int j = 0; j < window.GetLength(1); j++)
                {
                    // get absolute coordinates
                    int coordinateX = x + i - radius;
                    int coordinateY = y + j - radius;

                    double val = ColorCloseness((int)window[i, j], pixelVal) * SpatialCloseness(new Point(coordinateX, coordinateY), new Point(x, y));
                    numerator += (window[i,j] * val);
                    denominator += val;
                }
            }
            if (denominator == 0)
                return 0;
            return (int)(numerator / denominator);
        }

        public Bitmap Transform()
        {
            Bitmap newBitmap = new Bitmap(image.Width, image.Height);
            for (int i = 0; i < image.Width; i++)
            {
                for (int j = 0; j < image.Height; j++)
                {
                    Color original = image.GetPixel(i, j);
                    newBitmap.SetPixel(i, j,
                        Color.FromArgb(
                        CalcVal(i, j, Channel.Red),
                        CalcVal(i, j, Channel.Green),
                        CalcVal(i, j, Channel.Blue)));
                }
            }
            return newBitmap;
        }
    }
}
