﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace habibuszka.Transformations
{
    public class NoiseMedian : Transformation
    {
        private int radius;
        public NoiseMedian(Bitmap image, int radius)
            : base (image)
        {
            this.radius = radius;
        }

        private int GetMedian(int x, int y, Channel channel)
        {
            int size = (radius * 2) + 1;
            double[,] window = GetWindow(x, y, size, channel);
            List<double> temp = window.Cast<double>().ToList();
            temp.Sort();
            return (int)temp[temp.Count / 2];
        }

        public Bitmap Transform()
        {
            Bitmap newBitmap = new Bitmap(image.Width, image.Height);
            for (int i = 0; i < image.Width; i++)
            {
                for (int j = 0; j < image.Height; j++)
                {
                    Color original = image.GetPixel(i, j);
                    newBitmap.SetPixel(i, j, 
                        Color.FromArgb(
                        GetMedian(i, j, Channel.Red), 
                        GetMedian(i, j, Channel.Green), 
                        GetMedian(i, j, Channel.Blue)));
                }
            }
            return newBitmap;
        }
    }
}
