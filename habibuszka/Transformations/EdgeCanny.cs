﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace habibuszka.Transformations
{
    public class EdgeCanny : Convolution
    {
        public EdgeCanny(Bitmap bitmap)
            : base(bitmap)
        { }

        public Bitmap Transform(int lowerThreshold, int UpperThreshold)
        {
            Bitmap newBitmap = new Bitmap(bitmap.Width, bitmap.Height);
            for (int i = 0; i < bitmap.Width; i++)
            {
                for (int j = 0; j < bitmap.Height; j++)
                {
                    newBitmap.SetPixel(i, j, Color.Black);
                }
            }

            GrayscaleOperation gray = new GrayscaleOperation(bitmap);
            Bitmap grayBitmap = gray.Execute();
            Convolution c = new Convolution(grayBitmap);
            Bitmap gaussianBitmap = c.Convolute(GaussianBlur.GetMask(3, 1.6), Mode.RepeatEdge);
            EdgeSobel edge = new EdgeSobel(gaussianBitmap);
            double[,] Gx = edge.RawHorizontalDetection();
            double[,] Gy = edge.RawVerticalDetection();

            double[,] magnitude = new double[gaussianBitmap.Width, gaussianBitmap.Height];
            double[,] orientation = new double[gaussianBitmap.Width, gaussianBitmap.Height];

            for (int i = 0; i < gaussianBitmap.Width; i++)
            {
                for (int j = 0; j < gaussianBitmap.Height; j++)
                {
                    magnitude[i, j] = Math.Sqrt(Math.Pow(Gx[i, j], 2) + Math.Pow(Gy[i, j], 2));
                    orientation[i, j] = (int)((Math.Atan2(Gy[i, j], Gx[i, j]) / (Math.PI)) * 180 + 360) % 360;
                }
            }

            // początkowy zbiór krawędzi
            List<Point> initialEdgeSet = new List<Point>();

            //inicjalizacja tabeli pomocniczej do okreslania kierunku gradientu
            int[] gradientOrientationTable = SetGradientOrientationsTable();

            for (int i = 1; i < gaussianBitmap.Width - 1; i++)
            {
                for (int j = 1; j < gaussianBitmap.Height - 1; j++)
                {
                    int o = (int)orientation[i, j];
                    double m = magnitude[i, j];

                    //określanie punktów sąsiadujących będących na linii prostopadłej do kierunku gradientu piksela
                    List<Point> edgePoints = GetEdgePoints(gradientOrientationTable[o]);

                    //moc punktów sąsiadujących
                    double neighbour1 = magnitude[i + edgePoints[0].X, j + edgePoints[0].Y];
                    double neighbour2 = magnitude[i + edgePoints[1].X, j + edgePoints[1].Y];

                    if (m > neighbour1 && m > neighbour2 && m > UpperThreshold)
                    {
                        newBitmap.SetPixel(i, j, Color.FromArgb(255, 255, 255));
                        initialEdgeSet.Add(new Point(i, j));                        
                    }
                }
            }

            //progowanie z histerezą
            while (initialEdgeSet.Count == 0)
	        {
                Point point = initialEdgeSet[initialEdgeSet.Count - 1];
                initialEdgeSet.Remove(point);
		        int o = (int)orientation[point.X, point.Y];
		        Point[] tempNeighPoints = new Point[8] {
			        new Point(-1,0),
			        new Point(-1,1),
			        new Point(0,-1),
			        new Point(0,1),
			        new Point(1,-1),
			        new Point(1,0),
			        new Point(1,1),
                    new Point(-1,-1)
		        };

                foreach (Point direction in GetEdgePoints(gradientOrientationTable[o]))
		        {
                    Point currentPoint = point;
                    while(true)
			        {
                        Point nextPoint = new Point(currentPoint.X + direction.X, currentPoint.Y + direction.Y);
                        if (nextPoint.X == bitmap.Width - 1 || nextPoint.X == 0 || nextPoint.Y == bitmap.Height - 1 || nextPoint.Y == 0) 
                            break;
                        if (newBitmap.GetPixel(nextPoint.X, nextPoint.Y).R == 255) 
                            break;
                        if (magnitude[nextPoint.X, nextPoint.Y] < lowerThreshold) 
                            break;
                        if (gradientOrientationTable[(int) orientation[nextPoint.X, nextPoint.Y]] != gradientOrientationTable[o])
                            break;
                        bool maxMagnitude = true;
                        double m = magnitude[nextPoint.X, nextPoint.Y];
                        for (int i=0; i<8; i++)
				        {
                            Point tempNeigh = new Point(nextPoint.X + tempNeighPoints[i].X, nextPoint.Y + tempNeighPoints[i].Y);
                            if (tempNeigh.Y == currentPoint.X && tempNeigh.Y == currentPoint.Y) 
                                continue;
                            if (gradientOrientationTable[(int)orientation[tempNeigh.X, tempNeigh.Y]] != gradientOrientationTable[o]) 
                                continue;
                            if (magnitude[tempNeigh.X, tempNeigh.Y] >= m)
					        {
                                maxMagnitude = false;
                                break;
                            }
                        }
                        if (!maxMagnitude) break;
                        newBitmap.SetPixel(nextPoint.X, nextPoint.Y, Color.FromArgb(255, 255, 255));
                        currentPoint = nextPoint;
                    }
                }
            }
            return newBitmap;
        }

        private int[] SetGradientOrientationsTable()
        {
            int[] result = new int[361];
            for (int i = 0; i <= 360; i++)
            {
                if (i < 23 || (i >= 158 && i < 203) || (i >= 338 && i < 361))
                {
                    result[i] = 0;
                }
                else if ((i >= 23 && i < 68) || (i >= 203 && i < 248))
                {
                    result[i] = 1;
                }
                else if ((i >= 68 && i < 113) || (i >= 248 && i < 293))
                { 
                    result[i] = 2;
                }
                else if ((i >= 113 && i < 158) || (i >= 293 && i < 338))
                {
                    result[i] = 3;
                }
            }
            return result;
        }

        private List<Point> GetEdgePoints(int orientation)
        {
            List<Point> points = new List<Point>();
            if (orientation == 0)
            {
                points.Add(new Point(0, -1));
                points.Add(new Point(0, 1));
            }
            else if (orientation == 1)
            {
                points.Add(new Point(1, -1));
                points.Add(new Point(-1, 1));
            }
            else if (orientation == 2)
            {
                points.Add(new Point(-1, 0));
                points.Add(new Point(1, 0));
            }
            else if (orientation == 3)
            {
                points.Add(new Point(-1, -1));
                points.Add(new Point(1, 1));
            }
            return points;
        }
    }
}
