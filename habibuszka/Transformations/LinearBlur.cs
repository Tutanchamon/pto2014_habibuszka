﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace habibuszka.Transformations
{
    class LinearBlur
    {
        public static double[,] GetMask(double[,] matrix, bool normalize)
        {
            if (normalize)
            {
                double sum = Convolution.Sum(matrix);
                for (int i = 0; i < matrix.GetLength(0); i++)
                {
                    for (int j = 0; j < matrix.GetLength(1); j++)
                    {
                        matrix[i, j] /= sum;
                    }
                }
            }
                return matrix;
        }

    }
}
