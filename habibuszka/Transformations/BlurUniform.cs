﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace habibuszka.Transformations
{
    public class BlurUniform
    {
        public static double[,] GetMask(int size)
        {
            double[,] matrix = new double[2 * size + 1, 2 * size + 1];
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    matrix[i, j] = 1;
                }
            }
            return matrix;
        }
    }
}
