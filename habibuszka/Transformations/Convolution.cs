﻿using habibuszka.Exceptions;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace habibuszka.Transformations
{
    public class Convolution
    {
        protected Bitmap bitmap;

        public Convolution(Bitmap bitmap)
        {
            this.bitmap = bitmap;
        }

        public static double Sum(double[,] matrix)
        {
            double sum = 0;
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    sum += matrix[i, j];
                }
            }
                return sum;
        }

        public static double[,] Join(double[,] matrixA, double[,] matrixB)
        {
            double[,] matrixC = new double[matrixA.GetLength(0), matrixA.GetLength(1)];

            for (int i = 0; i < matrixA.GetLength(0); i++)
            {
                for (int j = 0; j < matrixA.GetLength(1); j++)
                {
                    matrixC[i, j] = matrixA[i, j] * matrixB[i, j];
                }
            }
            return matrixC;
        }

        public static double[,] GetMask(int size, Mode mode) 
        {
            double[,] matrix = new double[2 * size + 1, 2 * size + 1];
            matrix[size, size] = 1;
            return matrix;
        }

        public static double[,] Reflection(double[,] original)
        {
            int iSize = original.GetLength(0) - 1;
            int jSize = original.GetLength(1) - 1;
            double[,] reflection = new double[original.GetLength(0), original.GetLength(1)];
            for (int i = 0; i <= iSize; i++)
            {
                for (int j = 0; j <= jSize; j++)
                {
                    reflection[iSize - i, jSize - j] = original[i, j];
                }
            }
            return reflection;
        }

        public Bitmap Convolute(double[,] mask, Mode mode = Mode.RepeatEdge)
        {
            double weight = Sum(mask);
            Transformation t = new Transformation(bitmap);
            Bitmap newBitmap = new Bitmap(bitmap.Width, bitmap.Height);

            for (int x = 0; x < bitmap.Width; x++)
            {
                for (int y = 0; y < bitmap.Height; y++)
                {
                    int red = 0;
                    int green = 0;
                    int blue = 0;
                    foreach (Channel channel in (Channel[])Enum.GetValues(typeof(Channel)))
                    {
                        double[,] window = t.GetWindow(x, y, mask.GetLength(0), channel, mode);
                        double[,] acc = Join(window, Reflection(mask));
                        double sumAcc = Sum(acc);
                        if (weight != 0)
                            sumAcc = sumAcc / (double)weight;
                        if (sumAcc > 255)
                            sumAcc = 255;
                        if (sumAcc < 0)
                            sumAcc = 0;

                        switch (channel)
                        {
                            case Channel.Red:
                                red = (int)sumAcc;
                            break;

                            case Channel.Green:
                                green = (int)sumAcc;
                            break;

                            case Channel.Blue:
                                blue = (int)sumAcc;
                            break;
                        }
                    }
                    newBitmap.SetPixel(x, y, Color.FromArgb(red, green, blue));
                }
            }
            return newBitmap;
        }
    }

}
