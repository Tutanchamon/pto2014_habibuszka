﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace habibuszka.Transformations
{
    class Point2d
    {
        public double x { get; set; }
        public double y { get; set; }

        public Point2d() { }

        public Point2d(double x, double y)
        {
            this.x = x;
            this.y = y;
        }
    }
}
