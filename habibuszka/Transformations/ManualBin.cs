﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace habibuszka.Transformations
{
    public class ManualBin
    {
        protected Bitmap image;
        public Bitmap modifiedImage;
        public ManualBin(Image image)
        {
            this.image = new Bitmap(image);
        }

        public Bitmap Execute(int threshold)
        {
            Bitmap newBitmap = new Bitmap(image.Width, image.Height);
            for (int i=0; i < image.Width; i++)
            {
                for (int j=0; j < image.Height; j++)
                {
                    Color original = image.GetPixel(i, j);
                    int val = (original.R + original.G + original.B) / 3;
                    Color newColor;
                    if (val >= threshold) 
                        newColor = Color.FromArgb(255, 255, 255);
                    else
                        newColor = Color.FromArgb(0, 0, 0);
                    newBitmap.SetPixel(i, j, newColor);
                }
            }
            return newBitmap;
        }
    }
}
