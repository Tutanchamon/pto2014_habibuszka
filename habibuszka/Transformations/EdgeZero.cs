﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace habibuszka.Transformations
{
    public class EdgeZero : Convolution
    {
        public EdgeZero(Bitmap bitmap)
            : base(bitmap)
        { }

        public Bitmap Transform(int size, double sigma, int t)
        {
            EdgeLaplaceOfGauss el = new EdgeLaplaceOfGauss(bitmap);
            Bitmap laplacian = el.Convolute(el.GetMask(size, sigma));
            Transformation transformation = new Transformation(laplacian);
            Bitmap newBitmap = new Bitmap(bitmap.Width, bitmap.Height);
            int v0 = 128;

            for (int x = 0; x < bitmap.Width; x++)
            {
                for (int y = 0; y < bitmap.Height; y++)
                {
                    int red = 0;
                    int green = 0;
                    int blue = 0;
                    foreach (Channel channel in (Channel[])Enum.GetValues(typeof(Channel)))
                    {
                        double[,] window = transformation.GetWindow(x, y, size, channel);
                        if (GetMin(window) < v0 - t && GetMax(window) > v0 + t)
                        {
                            switch (channel)
                            {
                                case Channel.Red:
                                    red = laplacian.GetPixel(x, y).R;
                                    break;

                                case Channel.Green:
                                    green = laplacian.GetPixel(x, y).G;
                                    break;

                                case Channel.Blue:
                                    blue = laplacian.GetPixel(x, y).B;
                                    break;
                            }
                        }
                        
                    }
                    newBitmap.SetPixel(x, y, Color.FromArgb(red, green, blue));
                }
            }
            return newBitmap;
        }

        private double GetMax(double[,] window)
        {
            double max = 0;
            for (int x = 0; x < window.GetLength(0); x++)
            {
                for (int y = 0; y < window.GetLength(1); y++)
                {
                    if (window[x, y] > max)
                        max = window[x, y];
                }
            }
            return max;
        }
        private double GetMin(double[,] window)
        {
            double min = 255;
            for (int x = 0; x < window.GetLength(0); x++)
            {
                for (int y = 0; y < window.GetLength(1); y++)
                {
                    if (window[x, y] < min)
                        min = window[x, y];
                }
            }
            return min;
        }

    }
}
