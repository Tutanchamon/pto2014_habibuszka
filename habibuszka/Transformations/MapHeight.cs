﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace habibuszka.Transformations
{
    public class MapHeight : Transformation
    {
        public MapHeight(Image image)
            : base(image)
        { }

        public Bitmap Transform()
        {
            GrayscaleOperation gray = new GrayscaleOperation(image);
            return gray.Execute();
        }
    }
}
