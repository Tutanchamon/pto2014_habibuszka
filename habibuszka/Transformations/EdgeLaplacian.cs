﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace habibuszka.Transformations
{
    public class EdgeLaplacian : Convolution
    {
        public EdgeLaplacian(Bitmap bitmap)
            : base(bitmap)
        { }

        public double[,] GetMask(int size, Mode mode = Mode.RepeatEdge)
        {
            double[,] matrix = new double[size, size];
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    if(i == (size / 2) && j == (size / 2))
                        matrix[i, j] = (size * size) - 1;
                    else
                        matrix[i, j] = -1;
                }
            }
            return matrix;
        }
    }
}
