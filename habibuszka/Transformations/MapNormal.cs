﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace habibuszka.Transformations
{
    public class MapNormal : Convolution
    {
        public MapNormal(Bitmap image)
            : base(image)
        { }

        public Bitmap Transform(double strength)
        {
            Bitmap newBitmap = new Bitmap(bitmap.Width, bitmap.Height);
            MapHeight mapHeight = new MapHeight(bitmap);
            EdgeSobel edge = new EdgeSobel(mapHeight.Transform());
            double[,] Gx = edge.RawHorizontalDetection();
            double[,] Gy = edge.RawVerticalDetection();

            for (int i = 0; i < bitmap.Width; i++)
            {
                for (int j = 0; j < bitmap.Height; j++)
                {
                    double dx = Gx[i, j] / 255;
                    double dy = Gy[i, j] / 255;
                    double dz = 1 / strength;

                    double length = Math.Sqrt(Math.Pow(dx, 2) + Math.Pow(dy, 2) + Math.Pow(dz, 2));

                    //normalize and scale to [0, 255]
                    dx = ((dx / length) + 1) * 127.5;
                    dy = ((dy / length) + 1) * 127.5;
                    dz = ((dz / length) + 1) * 127.5;

                    newBitmap.SetPixel(i, j, Color.FromArgb((int)dx, (int)dy, (int)dz));
                }
            }
            return newBitmap;
        }
    }
}
