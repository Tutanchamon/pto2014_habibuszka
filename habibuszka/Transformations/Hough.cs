﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace habibuszka.Transformations
{
    public class Hough : Convolution
    {
        public Hough(Bitmap bitmap) : base(bitmap) { }

        public Bitmap Transform(int thetaDensity, bool skipEdgeDetection)
        {
            Bitmap newBitmap = new Bitmap(bitmap.Width, bitmap.Height);
            newBitmap = new GrayscaleOperation(bitmap).Execute();
            if (!skipEdgeDetection)
            {
                EdgeLaplacian edge = new EdgeLaplacian(newBitmap);
                newBitmap = edge.Convolute(edge.GetMask(3));
            }
            int roMax = (int) Math.Sqrt(newBitmap.Width * newBitmap.Width + newBitmap.Height * newBitmap.Height);
            int thetaSize = 180 * thetaDensity;
            Bitmap toReturn = new Bitmap(thetaSize, roMax * 2 + 1);
            int[,] hough = new int[thetaSize, roMax * 2 + 1];
            for (int i = 0; i < newBitmap.Width; i++)
            {
                for (int j = 0; j < newBitmap.Height; j++)
                {
                    Color original = newBitmap.GetPixel(i, j);
                    if (original.R > 0)
                    {
                        for (int k=0; k<thetaSize; k++)
					    {
						    double theta = (k * Math.PI)/(180 * thetaDensity);
						    int ro = (int) (i * Math.Cos(theta) + j * Math.Sin(theta));
						    int houghValue = hough[k, ro + roMax];
                            hough[k, ro + roMax] += 1;
					    }
                    }
                }
            }
            // normalizacja
            int max = 0;
            for (int i = 0; i < hough.GetLength(0); i++)
            {
                for (int j = 0; j < hough.GetLength(1); j++)
                {
                    if (hough[i, j] > max)
                    {
                        max = hough[i, j];
                    }
                }
            }
            // set the new image
            for (int i = 0; i < toReturn.Width; i++)
            {
                for (int j = 0; j < toReturn.Height; j++)
                {
                    int valueToSet = (hough[i, j] * 255) / max;
                    toReturn.SetPixel(i, j, Color.FromArgb(valueToSet, valueToSet, valueToSet));
                }
            }

                return toReturn;
        }
    }
}
