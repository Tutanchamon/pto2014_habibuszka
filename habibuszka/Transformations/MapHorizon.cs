﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace habibuszka.Transformations
{
    public class MapHorizon : Transformation
    {
        public MapHorizon(Image image)
            : base(image)
        { }

        public Bitmap Transform(double scale, int sunAlpha, Direction direction)
        {
            Bitmap newBitmap = new Bitmap(image.Width, image.Height);
            MapHeight mapHeight = new MapHeight(image);
            Bitmap map = mapHeight.Transform();
            int dx = 0;
            int dy = -1;
            switch (direction)
            {
                case Direction.North: dx = 0; dy = -1; break;
                case Direction.South: dx = 0; dy = 1; break;
                case Direction.East: dx = 1; dy = 0; break;
                case Direction.West: dx = -1; dy = 0; break;
            }

            for (int i = 0; i < image.Width; i++)
            {
                for (int j = 0; j < image.Height; j++)
                {
                    double alpha = 0.0;
                    int currentH = map.GetPixel(i, j).R;
                    for (int k = i + dx, l = j + dy; 
                        (k < image.Width && k > 0 && l < image.Height && l > 0); 
                        k += dx, l += dy)
                    {
                        int rayH = map.GetPixel(k, l).R;
                        if (currentH < rayH)
                        {
                            double dist = Math.Sqrt(Math.Pow(k - i, 2) + Math.Pow(l - j, 2)) * scale;
                            double rayAlpha = Math.Atan((rayH - currentH) / dist);
                            if (rayAlpha > alpha)
                                alpha = rayAlpha;
                        }
                    }
                    double delta = alpha - sunAlpha * Math.PI / 180.0;
                    if (delta > 0)
                    {
                        double value = Math.Cos(delta) * 255;
                        newBitmap.SetPixel(i, j, Color.FromArgb((int)value, (int)value, (int)value));
                    }
                    else
                        newBitmap.SetPixel(i, j, Color.FromArgb(255, 255, 255));
                }
            }
            return newBitmap;
        }

        public enum Direction
        {
            North,
            South,
            East, 
            West
        }
    }
}
