﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace habibuszka.Transformations.Segmentation
{
    public class Queue
    {
        List<SegmentationPixel> queue = new List<SegmentationPixel>();

        public int Count
        {
            get { return queue.Count; }
        }

        public void AddToEnd(SegmentationPixel p)
        {
            queue.Add(p);
        }

        public SegmentationPixel RemoveAtFront()
        {
            SegmentationPixel temp = queue[0];
            queue.RemoveAt(0);
            return temp;
        }

        public bool IsEmpty
        {
            get { return (queue.Count == 0); }
        }

        public override string ToString()
        {
            return base.ToString() + " Count = " + queue.Count.ToString();
        }
    }
}
