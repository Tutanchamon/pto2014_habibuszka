﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace habibuszka.Transformations.Segmentation
{
    public class SegmentationPixel
    {
        public int X;
        public int Y;
        public int Height;
        public int Label;
        public int Distance;

        public SegmentationPixel()
        {
            this.X = -1;
            this.Y = -1;
            this.Height = -1000;
            this.Label = -1000;
            this.Distance = -1000;
        }

        public SegmentationPixel(int x, int y)
        {
            this.X = x;
            this.Y = y;
            this.Height = -1000;
            this.Label = Segmentation.INIT;
            this.Distance = 0;
        }

        public SegmentationPixel(int x, int y, int height)
        {
            this.X = x;
            this.Y = y;
            this.Height = height;
            this.Label = Segmentation.INIT;
            this.Distance = 0;
        }

        public override bool Equals(Object obj)
        {
            SegmentationPixel p = (SegmentationPixel)obj;
            return (X == p.X && X == p.Y);
        }

        public override int GetHashCode()
        {
            return X;
        }
        public override string ToString()
        {
            return "Height = " + Height + "; X = " + X.ToString() + ", Y = " + Y.ToString() +
                   "; Label = " + Label.ToString() + "; Distance = " + Distance.ToString();
        }
    }
}
