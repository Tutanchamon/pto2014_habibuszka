﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace habibuszka.Transformations.Segmentation
{
    public class Segmentation
    {
        public const int INIT = -1;
        public const int MASK = -2;
        public const int WSHED = 0;

        private Bitmap bitmap;
        private Bitmap grayscale;
        private SegmentationPixel FictitiousPixel = new SegmentationPixel();
        private int currentLabel = 0;
        private int currentDistance = 0;
        private Queue queue = new Queue();
        private List<List<SegmentationPixel>> heightSortedList;
        private Dictionary<string, SegmentationPixel> pixelMap;
        private int watershedPixelCount = 0;

        public Segmentation(Bitmap bitmap)
        {
            this.bitmap = bitmap;
            GrayscaleOperation go = new GrayscaleOperation(bitmap);
            grayscale = go.Execute();

            heightSortedList = new List<List<SegmentationPixel>>(256);
            for (int i = 0; i < 256; i++)
                heightSortedList.Add(new List<SegmentationPixel>());
        }

        private void CreatePixelMapAndHeightSortedArray()
        {
            pixelMap = new Dictionary<string, SegmentationPixel>(bitmap.Width * bitmap.Height);

            // mapa pikseli i lista pikseli o określonej wysokości (wartość w grayscale)
            for (int y = 0; y < bitmap.Height; y++)
            {
                for (int x = 0; x < bitmap.Width; x++)
                {
                    int height = grayscale.GetPixel(x, y).R;
                    SegmentationPixel p = new SegmentationPixel(x, y, height);
                    
                    pixelMap.Add(p.X.ToString() + "," + p.Y.ToString(), p);
                    heightSortedList[height].Add(p);
                }
            }
            this.currentLabel = 0;
        }

        public Bitmap Transform()
        {
            CreatePixelMapAndHeightSortedArray();
            Segment();
            DrawLines();
            return (Bitmap)bitmap.Clone();
        }

        private void Segment()
        {
            for (int h = 0; h < heightSortedList.Count; h++)
            {
                foreach (SegmentationPixel heightSortedPixel in heightSortedList[h])
                {
                    heightSortedPixel.Label = MASK;
                    
                    List<SegmentationPixel> neighbouringPixels = GetNeighbouringPixels(heightSortedPixel);
                    
                    foreach (SegmentationPixel neighbouringPixel in neighbouringPixels)
                    {
                        if (neighbouringPixel.Label > 0 || neighbouringPixel.Label == WSHED)
                        {
                            heightSortedPixel.Distance = 1;
                            queue.AddToEnd(heightSortedPixel);
                            break;
                        }
                    }
                }
                currentDistance = 1;
                queue.AddToEnd(FictitiousPixel);
                
                while (true)
                {
                    SegmentationPixel p = queue.RemoveAtFront();
                    if (p.Equals(FictitiousPixel))
                    {
                        if (queue.IsEmpty)
                            break;
                        else
                        {
                            queue.AddToEnd(FictitiousPixel);
                            currentDistance++;
                            p = queue.RemoveAtFront();
                        }
                    }

                    List<SegmentationPixel> neighbouringPixels = GetNeighbouringPixels(p);
                    foreach (SegmentationPixel neighbouringPixel in neighbouringPixels)
                    {
                        if (neighbouringPixel.Distance <= currentDistance &&
                            (neighbouringPixel.Label > 0 || neighbouringPixel.Label == WSHED))
                        {
                            if (neighbouringPixel.Label > 0)
                            {
                                if (p.Label == MASK)
                                    p.Label = neighbouringPixel.Label;
                                else if (p.Label != neighbouringPixel.Label)
                                {
                                    p.Label = WSHED;
                                    watershedPixelCount++;
                                }
                            }
                            else if (p.Label == MASK)
                            {
                                p.Label = WSHED;
                                watershedPixelCount++;
                            }
                        }
                        else if (neighbouringPixel.Label == MASK && neighbouringPixel.Distance == 0)
                        {
                            neighbouringPixel.Distance = currentDistance + 1;
                            queue.AddToEnd(neighbouringPixel);
                        }
                    }
                }
                foreach (SegmentationPixel p in heightSortedList[h])
                {
                    p.Distance = 0;
                    
                    if (p.Label == MASK)
                    {
                        currentLabel++;
                        p.Label = currentLabel;
                        queue.AddToEnd(p);
                        while (!queue.IsEmpty)
                        {
                            SegmentationPixel q = queue.RemoveAtFront();
                            
                            List<SegmentationPixel> neighbouringPixels = GetNeighbouringPixels(q);
                            foreach (SegmentationPixel neighbouringPixel in neighbouringPixels)
                            {
                                if (neighbouringPixel.Label == MASK)
                                {
                                    neighbouringPixel.Label = currentLabel;
                                    queue.AddToEnd(neighbouringPixel);
                                }
                            }
                        }
                    }
                }
            }
        }

        private List<SegmentationPixel> GetNeighbouringPixels(SegmentationPixel centerPixel)
        {
            List<SegmentationPixel> result = new List<SegmentationPixel>();
            // -1, -1                
            if ((centerPixel.X - 1) >= 0 && (centerPixel.Y - 1) >= 0)
                result.Add(pixelMap[(centerPixel.X - 1).ToString() + "," + (centerPixel.Y - 1).ToString()]);
            //  0, -1
            if ((centerPixel.Y - 1) >= 0)
                result.Add(pixelMap[centerPixel.X.ToString() + "," + (centerPixel.Y - 1).ToString()]);
            //  1, -1
            if ((centerPixel.X + 1) < bitmap.Width && (centerPixel.Y - 1) >= 0)
                result.Add(pixelMap[(centerPixel.X + 1).ToString() + "," + (centerPixel.Y - 1).ToString()]);
            // -1, 0
            if ((centerPixel.X - 1) >= 0)
                result.Add(pixelMap[(centerPixel.X - 1).ToString() + "," + centerPixel.Y.ToString()]);
            //  1, 0
            if ((centerPixel.X + 1) < bitmap.Width)
                result.Add(pixelMap[(centerPixel.X + 1).ToString() + "," + centerPixel.Y.ToString()]);
            // -1, 1
            if ((centerPixel.X - 1) >= 0 && (centerPixel.Y + 1) < bitmap.Height)
                result.Add(pixelMap[(centerPixel.X - 1).ToString() + "," + (centerPixel.Y + 1).ToString()]);
            //  0, 1
            if ((centerPixel.Y + 1) < bitmap.Height)
                result.Add(pixelMap[centerPixel.X.ToString() + "," + (centerPixel.Y + 1).ToString()]);
            //  1, 1
            if ((centerPixel.X + 1) < bitmap.Width && (centerPixel.Y + 1) < bitmap.Height)
                result.Add(pixelMap[(centerPixel.X + 1).ToString() + "," + (centerPixel.Y + 1).ToString()]);

            return result;
        }

        private void DrawLines()
        {
            if (watershedPixelCount == 0)
                return;

            for (int y = 0; y < bitmap.Height; y++)
            {
                for (int x = 0; x < bitmap.Width; x++)
                {
                    if (pixelMap[x.ToString() + "," + y.ToString()].Label == WSHED)
                        bitmap.SetPixel(x, y, Color.FromArgb(0, 0, 0));
                }
            }
        }
    }
}
