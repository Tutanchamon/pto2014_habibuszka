﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace habibuszka.Transformations
{
    public class EdgeGradient : Convolution
    {
        protected double[,] g_x;
        protected double[,] g_y;
        public EdgeGradient(Bitmap bitmap)
            : base(bitmap)
        { }

        private Bitmap VerticalDetection()
        {
            return Convolute(g_y);
        }

        private Bitmap HorizontalDetection()
        {
            return Convolute(g_x);
        }

        public Bitmap Transform()
        {
            Bitmap newBitmap = new Bitmap(bitmap.Width, bitmap.Height);
            Bitmap imageX = HorizontalDetection();
            Bitmap imageY = VerticalDetection();

            for (int x = 0; x < bitmap.Width; x++)
            {
                for (int y = 0; y < bitmap.Height; y++)
                {
                    int red = (int)Math.Sqrt(Math.Pow((double)imageX.GetPixel(x, y).R, 2.0) + Math.Pow((double)imageY.GetPixel(x, y).R, 2.0));
                    if (red > 255)
                        red = 255;
                    if (red < 0)
                        red = 0;
                    int green = (int)Math.Sqrt(Math.Pow((double)imageX.GetPixel(x, y).G, 2.0) + Math.Pow((double)imageY.GetPixel(x, y).G, 2.0));
                    if (green > 255)
                        green = 255;
                    if (green < 0)
                        green = 0;
                    int blue = (int)Math.Sqrt(Math.Pow((double)imageX.GetPixel(x, y).B, 2.0) + Math.Pow((double)imageY.GetPixel(x, y).B, 2.0));
                    if (blue > 255)
                        blue = 255;
                    if (blue < 0)
                        blue = 0;
                    newBitmap.SetPixel(x, y, Color.FromArgb(red, green, blue));
                }
            }
            return newBitmap;
        }
    }
}
