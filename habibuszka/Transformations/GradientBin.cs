﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace habibuszka.Transformations
{
    public class GradientBin : ManualBin
    {
        public GradientBin(Image image)
            : base(image)
        {
        }

        public int CalculateThreshold()
        {
            int sumA = 0;
            int sumB = 0;
            Bitmap newBitmap = new Bitmap(image.Width, image.Height);
            for (int i=0; i < image.Width; i++)
            {
                for (int j=0; j < image.Height; j++)
                {
                    int gx = GetGradientX(i, j);
                    int gy = GetGradientY(i, j);
                    int maxG = Math.Max(gx, gy);
                    sumA += maxG * GetPixelValue(i, j);
                    sumB += maxG;
                }
            }
            return sumA / sumB;
        }

        private int GetPixelValue(int x, int y)
        {
            if (x < 0)
                x = 0;
            if (x > image.Width - 1)
                x = image.Width - 1;
            if (y < 0)
                y = 0;
            if (y > image.Height - 1)
                y = image.Height - 1;
            Color original = image.GetPixel(x, y);
            int val = (original.R + original.G + original.B) / 3;
            return val;
        }

        private int GetGradientX(int x, int y)
        {
            int result = GetPixelValue(x + 1, y) - GetPixelValue(x - 1, y);
            return result;
        }

        private int GetGradientY(int x, int y)
        {
            int result = GetPixelValue(x, y + 1) - GetPixelValue(x, y - 1);
            return result;
        }
    }
}
