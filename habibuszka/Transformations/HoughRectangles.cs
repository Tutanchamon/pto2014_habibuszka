﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace habibuszka.Transformations
{
    public class HoughRectangles : Convolution
    {
        public HoughRectangles(Bitmap bitmap) : base(bitmap) { }

        /**
         * 1. Wykryj krawędzie
         * 2. Wykryj narozniki
         * 3. Polacz wszystkie narozniki liniami
         * 4. Sprawdz, ktore katy miedzy liniami sa proste 
         * 5. Dla kazdego katu prostego, jesli linie lezaly na krawedziach, narysuj je
         */
        public Bitmap Transform()
        {
            Image resultImage = new Bitmap(bitmap.Width, bitmap.Height);
            using (Graphics grp = Graphics.FromImage(resultImage))
            {
                grp.FillRectangle(
                    Brushes.Black, 0, 0, bitmap.Width, bitmap.Height);
            }
            Bitmap finalBitmap = (Bitmap)resultImage.Clone();
            // Wykryj krawedzie
            Bitmap newBitmap = (Bitmap)bitmap.Clone();
            EdgeLaplacian edge = new EdgeLaplacian(bitmap);
            Bitmap edgeBitmap = edge.Convolute(edge.GetMask(3));
            GradientBin bin = new GradientBin(edgeBitmap);
            Bitmap binBitmap = bin.Execute(150);

            HoughLines houghLines = new HoughLines(newBitmap);
            Bitmap linesBitmap = houghLines.Transform(3, false);
            // Wykryj narożniki
            Point2d center = new Point2d(0, 0);
            CornerHarris harris = new CornerHarris(newBitmap);
            newBitmap = harris.Transform(30000000, 1.0, 0.76, 0.05);
            List<Point> corners = harris.foundPoints;
            Console.WriteLine("Liczba znalezionych wierzcholkow: " + corners.Count);
            List<PointLine> lines = new List<PointLine>();
            for (int i = 0; i < corners.Count; i++)
            {
                for (int j = i + 1; j < corners.Count; j++)
                {
                    lines.Add(new PointLine(corners[i].X, corners[i].Y, corners[j].X, corners[j].Y));
                }
            }
                Console.WriteLine("Liczba znalezionych linii: " + lines.Count);
                Graphics graphics = Graphics.FromImage(newBitmap);
                Pen pen = new Pen(Color.Blue, 1);
                for (int i = 0; i < corners.Count; i++) {
                    center.x += corners[i].X;
                    center.y += corners[i].Y;
                }
                center.x *= (1 / corners.Count);
                center.y *= (1 / corners.Count);
                // Sprawdz katy miedzy liniami
                for (int i = 0; i < lines.Count; i++)
                {
                    for (int j = i + 1; j < lines.Count; j++)
                    {
                        Double angle = Math.Atan2(lines[i].y2 - lines[i].y1, 
                            lines[i].x2 - lines[i].x1) - Math.Atan2(lines[j].y2 - lines[j].y1,
                            lines[j].x2 - lines[j].x2);
                        Double angleDeg = angle * (180.0 / Math.PI);
                        //Console.WriteLine("Kat miedzy liniami: " + angleDeg);
                        // Jesli kat prosty - rysuj linie
                        if ((angleDeg == 90) || (angleDeg == -90))
                        //if ((angleDeg > 88 && angleDeg < 92) || (angleDeg > -92 && angleDeg < -88))
                        {
                            graphics.DrawLine(pen, lines[i].x1, lines[i].y1, lines[i].x2, lines[i].y2);
                            graphics.DrawLine(pen, lines[j].x1, lines[j].y1, lines[j].x2, lines[j].y2);
                        }
                    }
                }

                // Porownaj obraz z wykrytymi krawedziami, z obrazem z liniami o kacie prostym i rysuj te linie, ktore sie nakladaja
                for (int x = 0; x < binBitmap.Width; x++)
                {
                    for (int y = 0; y < binBitmap.Height; y++)
                    {
                        if (newBitmap.GetPixel(x, y).B == 255 && linesBitmap.GetPixel(x, y).R == 255 && linesBitmap.GetPixel(x, y).B < 50 && linesBitmap.GetPixel(x, y).G < 50)
                        {
                            finalBitmap.SetPixel(x, y, newBitmap.GetPixel(x, y));
                        }
                    }
                }

                return finalBitmap;
        }

        private Point2d GetPointOfIntersection(PointLine pl1, PointLine pl2)
        {
            int x1 = pl1.x1; 
            int y1 = pl1.y1;
            int x2 = pl1.x2;
            int y2 = pl1.y2;
            int x3 = pl2.x1;
            int y3 = pl2.y1;
            int x4 = pl2.x2;
            int y4 = pl2.y2;

            double denom = ((x1 - x2) * (y3 - y4)) - ((y1 - y2) * (x3 - x4));
            if (Double.IsNaN(denom))
            {
                return new Point2d(-1, -1);
            }
            else
            {
                Point2d point = new Point2d();
                double x = (double) ((x1 * y2 - y1 * x2) * (x3 - x4) - (x1 - x2) * (x3 * y4 - y3 * x4)) / denom;
                double y = (double) ((x1 * y2 - y1 * x2) * (y3 - y4) - (y1 - y2) * (x3 * y4 - y3 * x4)) / denom;
                point.x = x;
                point.y = y;
                return point;
            }
        }
    }
}
