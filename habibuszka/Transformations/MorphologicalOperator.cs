﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace habibuszka.Transformations
{
    class MorphologicalOperator : Transformation
    {
        public MorphologicalOperator(Image image) : base(image) { }

        public Image Transform(int size, SE shape, MorpholigicalType type)
        {
            bool[,] window = GetSE(size, shape);
            Bitmap newBitmap = null;
            switch (type)
            {
                case MorpholigicalType.Close:
                    newBitmap = ErodeBitmap(window);
                    image = (Bitmap)newBitmap.Clone(); 
                    newBitmap = DilateBitmap(window);
                    break;
                case MorpholigicalType.Dilate:
                    newBitmap = DilateBitmap(window);
                    break;
                case MorpholigicalType.Erode:
                    newBitmap = ErodeBitmap(window);
                    break;
                case MorpholigicalType.Open:
                    newBitmap = DilateBitmap(window);
                    image = (Bitmap)newBitmap.Clone();
                    newBitmap = ErodeBitmap(window);
                    break;
            }
            
            return newBitmap;
        }

        public bool[,] GetSE(int size, SE shape)
        {
            switch (shape)
            {
                case SE.Square:
                    return SEsquare(size);
                case SE.Cross:
                    return SEcross(size);
                case SE.XCross:
                    return SExcross(size);
                case SE.HLine:
                    return SEhline(size);
                case SE.VLine:
                    return SEvline(size);
                default:
                    return SEsquare(size);
            }
        }

        public bool[,] SEsquare(int size)
        {
            bool[,] matrix = new bool[size, size];
            for (int i = 0; i < size; i++)
            {
                matrix[i, 0] = true;
                matrix[i, size - 1] = true;
            }
            for (int i = 1; i < size - 1; i++)
            {
                matrix[0, i] = true;
                matrix[size - 1, i] = true;
            }

                return matrix;

        }

        public bool[,] SEcross(int size)
        {
            bool[,] matrix = new bool[size, size];
            int center = size / 2;
            for (int i = 0; i < size; i++)
            {
                matrix[i, center] = true;
                matrix[center, i] = true;
            }
                return matrix;

        }

        public bool[,] SExcross(int size)
        {
            bool[,] matrix = new bool[size, size];
            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    if (i == j)
                        matrix[i, j] = true;
                    else if (i + j == 4)
                        matrix[i, j] = true;
                }
            }
                return matrix;

        }

        public bool[,] SEhline(int size)
        {
            bool[,] matrix = new bool[size, size];
            int center = size / 2;
            for (int i = 0; i < size; i++)
            {
                matrix[center, i] = true;
            }
                return matrix;

        }

        public bool[,] SEvline(int size)
        {
            bool[,] matrix = new bool[size, size];
            int center = size / 2;
            for (int i = 0; i < size; i++)
            {
                matrix[i, center] = true;
            }
            return matrix;

        }

        private int MorphDilate(int x, int y, Channel channel, bool[,] se)
        {
            int size = se.GetLength(0);
            double[,] window = GetWindow(x, y, size, channel);
            int min = 255;
            for (int i = 0; i < window.GetLength(0); i++)
            {
                for (int j = 0; j < window.GetLength(1); j++)
                {
                    if (se[i, j])
                    {
                        if (window[i, j] < min)
                        {
                            min = (int)window[i, j];
                        }
                    }
                }
            }
            return min;
        }

        private int MorphErode(int x, int y, Channel channel, bool[,] se)
        {
            int size = se.GetLength(0);
            double[,] window = GetWindow(x, y, size, channel);
            int max = 0;
            for (int i = 0; i < window.GetLength(0); i++)
            {
                for (int j = 0; j < window.GetLength(1); j++)
                {
                    if (se[i, j])
                    {
                        if (window[i, j] > max)
                        {
                            max = (int) window[i, j];
                        }
                    }
                }
            }
            return max;
        }

        private Bitmap ErodeBitmap(bool[,] window)
        {
            Bitmap newBitmap = new Bitmap(image.Width, image.Height);
            for (int x = 0; x < image.Width; x++)
            {
                for (int y = 0; y < image.Height; y++)
                {
                    newBitmap.SetPixel(x, y,
                        Color.FromArgb(
                        MorphErode(x, y, Channel.Red, window),
                        MorphErode(x, y, Channel.Green, window),
                        MorphErode(x, y, Channel.Blue, window)));
                }
            }
            return newBitmap;
        }

        private Bitmap DilateBitmap(bool[,] window)
        {
            Bitmap newBitmap = new Bitmap(image.Width, image.Height);
            for (int x = 0; x < image.Width; x++)
            {
                for (int y = 0; y < image.Height; y++)
                {
                    newBitmap.SetPixel(x, y,
                        Color.FromArgb(
                        MorphDilate(x, y, Channel.Red, window),
                        MorphDilate(x, y, Channel.Green, window),
                        MorphDilate(x, y, Channel.Blue, window)));
                }
            }
            return newBitmap;
        }
    }

    public enum MorpholigicalType
    {
        Erode,
        Dilate,
        Open,
        Close
    }
    public enum SE
    {
        Square,
        Cross,
        XCross,
        HLine,
        VLine
    }

}
