﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace habibuszka.Hist
{
    public class Histogram
    {
        private Bitmap bitmap;
        public Dictionary<int, int> rDict { get; set; }
        public Dictionary<int, int> gDict { get; set; }
        public Dictionary<int, int> bDict { get; set; }
        public Dictionary<int, int> lDict { get; set; }

        public Histogram(Bitmap bitmap)
        {
            this.bitmap = bitmap;
            rDict = new Dictionary<int, int>();
            gDict = new Dictionary<int, int>();
            bDict = new Dictionary<int, int>();
            lDict = new Dictionary<int, int>();
            fillDicts();
            Generate();
        }

        public void Generate()
        {
            for (int x = 0; x < bitmap.Width; x++)
            {
                for (int y = 0; y < bitmap.Height; y++)
                {
                    Color color = bitmap.GetPixel(x, y);
                    rDict[color.R] += 1;
                    gDict[color.G] += 1;
                    bDict[color.B] += 1;
                }
            }
            lDict = bDict;
        }
        private void fillDicts()
        {
            for (int i = 0; i < 256; i++)
            {
                rDict.Add(i, 0);
                gDict.Add(i, 0);
                bDict.Add(i, 0);
                lDict.Add(i, 0);
            }
        }

        private int maximumValue()
        {
            int max = rDict.Values.Max();
            if (max < gDict.Values.Max())
                max = gDict.Values.Max();
            if (max < bDict.Values.Max())
                max = bDict.Values.Max();
            return max;
        }

        public Bitmap GenerateHistogram(Dictionary<int, int> dict, Pen pen, Bitmap existingHistogram)
        { 
            int max = maximumValue();
            int histHeight = existingHistogram.Height - 10;
            Bitmap img = (Bitmap)existingHistogram;
            using (Graphics g = Graphics.FromImage(img))
            {
                for (int i = 0; i < dict.Count; i++)
                {
                    float pct = (float)dict[i] / max;
                    g.DrawLine(pen,
                        new Point(i, img.Height - 5),
                        new Point(i, img.Height - 5 - (int)(pct * histHeight))
                        );
                }
            }
            return img;
        }

        public Bitmap HistogramStretching()
        {
            int minR = GetMinEdge(rDict);
            int maxR = GetMaxEdge(rDict);
            int minG = GetMinEdge(gDict);
            int maxG = GetMaxEdge(gDict);
            int minB = GetMinEdge(bDict);
            int maxB = GetMaxEdge(bDict);
            int[] newValuesR = new int[256];
            int[] newValuesG = new int[256];
            int[] newValuesB = new int[256];
            for (int i = 0; i < 256; i++)
            {
                newValuesR[i] = (int)((255.0 / (double)(maxR - minR)) * (double)(i - minR));
                newValuesG[i] = (int)((255.0 / (double)(maxG - minG)) * (double)(i - minG));
                newValuesB[i] = (int)((255.0 / (double)(maxB - minB)) * (double)(i - minB));
            }

            Bitmap newBitmap = new Bitmap(bitmap.Width, bitmap.Height);
            for (int x = 0; x < bitmap.Width; x++)
            {
                for (int y = 0; y < bitmap.Height; y++)
                {
                    Color color = bitmap.GetPixel(x, y);
                    Color newColor = Color.FromArgb(
                        newValuesR[color.R],
                        newValuesG[color.G],
                        newValuesB[color.B]);
                    newBitmap.SetPixel(x, y, newColor);
                }
            }
            this.bitmap = newBitmap;
            rDict = new Dictionary<int, int>();
            gDict = new Dictionary<int, int>();
            bDict = new Dictionary<int, int>();
            lDict = new Dictionary<int, int>();
            fillDicts();
            Generate();
            return newBitmap;
        }

        private int GetGlobalMaxEdge()
        {
            int rMax = GetMaxEdge(rDict);
            int gMax = GetMaxEdge(gDict);
            int bMax = GetMaxEdge(bDict);
            return Math.Max(rMax, Math.Max(gMax, bMax));
        }

        private int GetMaxEdge(Dictionary<int, int> dict)
        {
            for (int i = dict.Count - 1; i >= 0; i--)
                if (dict[i] != 0)
                    return i;

            return dict.Count - 1;
        }

        private int GetGlobalMinEdge()
        {
            int rMin = GetMinEdge(rDict);
            int gMin = GetMinEdge(gDict);
            int bMin = GetMinEdge(bDict);
            return Math.Min(rMin, Math.Min(gMin, bMin));
        }

        private int GetMinEdge(Dictionary<int, int> dict)
        {
            for (int i = 0; i < dict.Count; i++)
                if (dict[i] != 0)
                    return i;

            return 0;
        }
        public Bitmap Equalize()
        {
            double[] normalizedR = GetNormalizedDistribute(rDict);
            double[] normalizedG = GetNormalizedDistribute(gDict);
            double[] normalizedB = GetNormalizedDistribute(bDict);

            Bitmap newBitmap = new Bitmap(bitmap.Width, bitmap.Height);
            for (int x = 0; x < bitmap.Width; x++)
            {
                for (int y = 0; y < bitmap.Height; y++)
                {
                    Color color = bitmap.GetPixel(x, y);
                    Color newColor = Color.FromArgb(
                        (int)normalizedR[color.R],
                        (int)normalizedG[color.G],
                        (int)normalizedB[color.B]);
                    newBitmap.SetPixel(x, y, newColor);
                }
            }
            this.bitmap = newBitmap;
            rDict = new Dictionary<int, int>();
            gDict = new Dictionary<int, int>();
            bDict = new Dictionary<int, int>();
            lDict = new Dictionary<int, int>();
            fillDicts();
            Generate();
            return newBitmap;
        }

        private double[] GetNormalizedDistribute(Dictionary<int, int> dict)
        {
            double[] probabilities = new double[256];
            double[] distribute = new double[256];
            for (int i = 0; i < probabilities.Length; i++)
            {
                probabilities[i] = (double)dict[i] / (double)(bitmap.Height * bitmap.Width);
            }
            for (int i = 0; i < distribute.Length; i++)
            {
                for (int j = 0; j < i; j++)
                {
                    distribute[i] += probabilities[j];
                }
            }
            return Normalize(distribute);
        }

        private double[] Normalize(double[] distribute)
        {
            double max = distribute.Max();
            double multiplier = 255 / max;
            double[] toReturn = new double[256];
            for (int i = 0; i < distribute.Length; i++)
            {
                toReturn[i] = distribute[i] * multiplier;
            }
            return toReturn;
        }
    }
}
