﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace habibuszka
{
    public abstract class AbstractOperation
    {
        protected Image loadedImage { get; set; }
        protected Bitmap bitmap { get; set; }
        public double previousValue { set; get; }
        public AbstractOperation(Image loadedImage)
        {
            UpdateImage(loadedImage, 0);
        }

        public void UpdateImage(Image image, int previousValue)
        {
            this.loadedImage = image;
            bitmap = new Bitmap(loadedImage);
            this.previousValue = (double)previousValue;
        }
        public void UpdateImage(Image image)
        {
            this.loadedImage = image;
            bitmap = new Bitmap(loadedImage);
        }


        public abstract Bitmap Execute();
    }
}
